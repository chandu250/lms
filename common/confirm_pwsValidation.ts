import { AbstractControl, ValidatorFn } from '@angular/forms';

export function EqualValidator(compareValue: string): ValidatorFn {

    return (control: AbstractControl): { [key: string]: boolean } | null => {

        if (control.value !== undefined
            && control.parent !== undefined
            && control.parent.controls !== undefined
            && control.parent.controls[compareValue] !== undefined
            && (control.value.trim() !== control.parent.controls[compareValue].value.trim())) {

            return { 'validateEqual': true };
        }
        return null;
    };
}
