import { CrudService } from '../services/crud.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import 'moment-timezone';
import { CONSTANTS } from './constants';
import * as SecureLS from 'secure-ls';

@Injectable()
export class GenericFunctions {

    myParams: any = {};

    constructor(private crudService: CrudService,
                private spinner: NgxSpinnerService, public router: Router) {
    }

    public setSecuredValue(key, value): void{
        const ls = new SecureLS({
            encodingType: 'des',
            isCompression: true,
            encryptionSecret: 'secret'
          });
        ls.set(key, value);
    }

    public getSecuredValue(key): any{    
        const ls = new SecureLS({
            encodingType: 'des',
            isCompression: true,
            encryptionSecret: 'secret'
          });
        return ls.get(key);
    }

    public clearSecuredValues(): void{
        const ls = new SecureLS({
            encodingType: 'des',
            isCompression: true,
            encryptionSecret: 'secret'
          });
        ls.clear();
    }

    public removeSecuredValue(key): void{
        const ls = new SecureLS({
            encodingType: 'des',
            isCompression: true,
            encryptionSecret: 'secret'
          });
        ls.remove(key);
    }

    public logOut(url): void {
        this.spinner.hide();
       // this.dialog.closeAll();
        localStorage.clear();
       // this.clearSecuredValues();
        this.router.navigate(['pages/auth/login'], { queryParams: { 
            page: url} });
    }

    public logOutWithParams(params): void {
        this.spinner.hide();
      //  this.dialog.closeAll();
        localStorage.clear();
       // this.clearSecuredValues();
        this.router.navigate(['pages/auth/login'], { queryParams: params });
    }

    public momentWithDate(date): any {
       if (date === null || date === undefined){
            return null;
        }else{
            return moment.utc(date).set({hour: 0, minute: 0, second: 0, millisecond: 0}).format();
        }
    }

    public momentWithoutUTC(date): any {
        if (date === null || date === undefined){
             return null;
         }else{
             return moment(date).format();
         }
     }

    public momentWithDateFormatYMD(date): any {
        if (date === null || date === undefined){
            return null;
        }else
        {
            return moment(date).format('YYYY-MM-DD');
        }
    }
    public momentWithDateFormatYM(date): any {
        if (date === null || date === undefined){
            return null;
        }else
        {
            return moment(date).format('YYYY-MM');
        }
    }

    public moment(): any {
        CONSTANTS.presentDate = localStorage.getItem('presentDate');
        // tslint:disable-next-line:max-line-length
        return moment.utc([CONSTANTS.presentDate.split('-')[2], +CONSTANTS.presentDate.split('-')[1] - 1, CONSTANTS.presentDate.split('-')[0]]).set({hour: 0, minute: 0, second: 0, millisecond: 0}).format();
    }

    public momentWeekday(): any {
        CONSTANTS.presentDate = localStorage.getItem('presentDate');
        // tslint:disable-next-line:max-line-length
        return moment.utc([CONSTANTS.presentDate.split('-')[2], +CONSTANTS.presentDate.split('-')[1] - 1, CONSTANTS.presentDate.split('-')[0]]).set({hour: 0, minute: 0, second: 0, millisecond: 0}).day();
    }

    public momentWeekdayWithDate(date): any {
        // tslint:disable-next-line:max-line-length
        return moment(date).set({hour: 0, minute: 0, second: 0, millisecond: 0}).day();
    }

    public momentWithTimeInHours(): any {
        return moment().format('HH');
    }

    public momentAfterDay(): any {
        CONSTANTS.presentDate = localStorage.getItem('presentDate');
        // tslint:disable-next-line:max-line-length
        return moment.utc([CONSTANTS.presentDate.split('-')[2], +CONSTANTS.presentDate.split('-')[1] - 1, +CONSTANTS.presentDate.split('-')[0] + 1]).set({hour: 0, minute: 0, second: 0, millisecond: 0}).format();
    }

    public momentBeforeDay(): any {
        CONSTANTS.presentDate = localStorage.getItem('presentDate');
        // tslint:disable-next-line:max-line-length
        return moment.utc([CONSTANTS.presentDate.split('-')[2], +CONSTANTS.presentDate.split('-')[1] - 1, +CONSTANTS.presentDate.split('-')[0]]).subtract(1, 'day').set({hour: 0, minute: 0, second: 0, millisecond: 0}).format();
    }

    public momentAfterDayWithDate(date): any {
        const tdate = moment(date).format('DD-MM-YYYY');
        // tslint:disable-next-line:max-line-length
        return moment.utc([tdate.split('-')[2], +tdate.split('-')[1] - 1, +tdate.split('-')[0] + 1]).set({hour: 0, minute: 0, second: 0, millisecond: 0}).format();
    }

    public momentBeforeDayWithDate(date): any {
        const d = new Date(date);
        const tdate = moment(d.setDate(d.getDate() - 1)).format();
        return tdate;
    }

    public momentEndDateWithTime(date): any {
        // tslint:disable-next-line:max-line-length
        return moment.utc(date).set({hour: 23, minute: 59, second: 59, millisecond: 0}).format();
    }

    public momentStartDateWithTime(date): any {
        // tslint:disable-next-line:max-line-length
        return moment.utc(date).set({hour: 0, minute: 0, second: 0, millisecond: 0}).format();
    }

    public momentWithTime(date): any {
        // tslint:disable-next-line:max-line-length
        return moment(date).set({hour: 0, minute: 0, second: 0, millisecond: 0}).format();
    }

    public momentWithHeadTime(date): any {
        // tslint:disable-next-line:max-line-length
        return moment(date).set({hour: 5, minute: 30, second: 0, millisecond: 0}).format();
    }

    public momentGetWeekday(): any {
        CONSTANTS.presentDate = localStorage.getItem('presentDate');
        // tslint:disable-next-line:max-line-length
        return moment.utc([CONSTANTS.presentDate.split('-')[2], +CONSTANTS.presentDate.split('-')[1] - 1, CONSTANTS.presentDate.split('-')[0]]).day();
    }

    public momentYMD(): any {
        CONSTANTS.presentDate = localStorage.getItem('presentDate');
        // tslint:disable-next-line:max-line-length
        return moment([CONSTANTS.presentDate.split('-')[2], +CONSTANTS.presentDate.split('-')[1] - 1, CONSTANTS.presentDate.split('-')[0]]).format('YYYY-MM-DD');
    }

    public momentYMDPresent(): any {
        CONSTANTS.presentDate = localStorage.getItem('presentDate');
        // tslint:disable-next-line:max-line-length
        return moment([CONSTANTS.presentDate.split('-')[2], +CONSTANTS.presentDate.split('-')[1] - 1, CONSTANTS.presentDate.split('-')[0]]).format('YYYY/MM/DD');
    }

    public momentFormatYMD(date): any {
        if (date === null || date === undefined){
            return null;
        }else{
            return moment(date).format('YYYY/MM/DD');
        }
    }

    public momentFormatM(date): any {
        if (date === null || date === undefined){
            return null;
        }else{
            return moment(date).set({hour: 0, minute: 0, second: 0, millisecond: 0}).format('MM');
        }
    }
  

    public momentFormatW(date): any {
        if (date === null || date === undefined){
            return null;
        }else{
            return moment(date).day();
        }
    }

    public momentFormatY(date): any {
        if (date === null || date === undefined){
            return null;
        }else{
            return moment(date).set({hour: 0, minute: 0, second: 0, millisecond: 0}).format('YYYY');
        }
    }

    public momentFormatYCur(): any {
        CONSTANTS.presentDate = localStorage.getItem('presentDate');
       // console.log(CONSTANTS.presentDate);
        // tslint:disable-next-line:max-line-length
        return moment([CONSTANTS.presentDate.split('-')[2], +CONSTANTS.presentDate.split('-')[1] - 1, CONSTANTS.presentDate.split('-')[0]]).format('YYYY');
    }

    public momentFormatYMD1(date): any {
        if (date === null || date === undefined){
            return null;
        }else{
            return moment(date).set({hour: 0, minute: 0, second: 0, millisecond: 0}).format('YYYY-MM-DD');
        }
    }

    public momentFormatYMDPrintView(date): any {
        if (date === null || date === undefined){
            return null;
        }else{
            return moment.utc(date).set({hour: 0, minute: 0, second: 0, millisecond: 0}).format('DD/MM/YYYY ');
        }
        
    }

    public momentFormatYMDPrintViewTime(date): any {
        if (date === null || date === undefined){
            return null;
        }else{
            return moment(date).format('dddd, MMMM Do YYYY, h:mm:ss a');
        }
        
    }

    public getCollegeCode(colleges, collegeId): any{
       if (colleges.filter(x => (x.collegeId === collegeId)).length > 0){
           return colleges.filter(x => (x.collegeId === collegeId))[0].collegeCode;
       } else{
           return '';
       }
    }

    public getAcademicYear(academicYears, academicYearId): any{
        if (academicYears.filter(x => (x.academicYearId === academicYearId)).length > 0){
            return academicYears.filter(x => (x.academicYearId === academicYearId))[0].academicYear;
        } else{
            return '';
        }
    }

    public getCourse(courses, courseId): any{
        if (courses.filter(x => (x.courseId === courseId)).length > 0){
            return courses.filter(x => (x.courseId === courseId))[0].courseCode;
        } else{
            return '';
        }
    }

    public getCourseGroup(courseGroups, courseGroupId): any{
        if (courseGroups.filter(x => (x.courseGroupId === courseGroupId)).length > 0){
            return courseGroups.filter(x => (x.courseGroupId === courseGroupId))[0].groupCode;
        } else{
            return '';
        }
    }

    public getCourseYear(courseYears, courseYearId): any{
        if (courseYears.filter(x => (x.courseYearId === courseYearId)).length > 0){
            return courseYears.filter(x => (x.courseYearId === courseYearId))[0].courseYearCode;
        } else{
            return '';
        }
    }

    public getSection(sections, groupSectionId): any{
        if (sections.filter(x => (x.groupSectionId === groupSectionId)).length > 0){
            return sections.filter(x => (x.groupSectionId === groupSectionId))[0].section;
        } else{
            return '';
        }
    }

    public getExam(exams, examId): any{
        if (exams.filter(x => (x.examId === examId)).length > 0){
            return exams.filter(x => (x.examId === examId))[0].examName;
        } else{
            return '';
        }
    }

    public getRegulations(regulations, regulationId): any{
        if (regulations.filter(x => (x.regulationId === regulationId)).length > 0){
            return regulations.filter(x => (x.regulationId === regulationId))[0].regulationCode;
        } else{
            return '';
        }
    }
    public dashboardHome(userTypeCode): any{
        if (userTypeCode === 'STAFF') {
            this.router.navigate(['/main-dashboard/main-dashboard']);
        } else 
        if (userTypeCode === 'STUDENT') {
            this.router.navigate(['/student-dashboard/student-dashboard']);
        }
    }
    // tslint:disable-next-line: typedef
    dataSecurity(){
        if (localStorage.getItem('isAdmin') === 'true' || localStorage.getItem('isPRINCIPAL') === 'true') {
            return true;
        }else
        if (localStorage.getItem('isAdmin') === 'false' || localStorage.getItem('isPRINCIPAL') === 'false') {
            return false;
        }
    }

    // tslint:disable-next-line: typedef
    dataSecurityLevel(){
        if (localStorage.getItem('isAdmin') === 'true') {
           return false;
          }else
          if (localStorage.getItem('isPRINCIPAL') === 'true'){
           return false;
        }else{
           
                return true;
            
               
        }
           
    }
    // tslint:disable-next-line: typedef
    dataSecurityLevelPrincipal(){
          if (localStorage.getItem('isAdmin') === 'true') {
            return false;
          }else{
            return true;
          }
          
           
    }

}
