import {CONSTANTS} from './constants';

declare let jQuery: any;

export class GlobalFunctions {
    scrollToTop = function (id) {
        setTimeout(function () {
            jQuery(id).scrollTop(0);
        }, 0);
    };

    copyObject = function (dataObj) {
        return JSON.parse(JSON.stringify(dataObj));
    };

    successErrorHandling = function (response, that, messageVariable) {
        that.snotifyService.clear();

        let messageText = '';
        messageText = response.message || CONSTANTS.message.INTERNAL_ERROR;

        if (response.code === CONSTANTS.errorCodes.UNAUTHORIZED ||
            response.code === CONSTANTS.errorCodes.TOKEN_EXPIRED ||
            response.code === CONSTANTS.errorCodes.TOKEN_REQUIRED) {
            localStorage.removeItem('token');
            localStorage.removeItem('token_type');
            localStorage.removeItem('loginUser');
            that.snotifyService.error(messageText, 'Oops..!');
           // window.location.href = '/login';
        }
        else {
            that.snotifyService.error(messageText, 'Oops..!');
        }
    };

    errorHanding = function (errorResponse, that, messageVariable, isSingleErrorReturn: boolean = false) {
        // let error = errorResponse.json();
        const error = errorResponse;

        let messageText = '';
        messageText = error.message || CONSTANTS.message.INTERNAL_ERROR;
        that.snotifyService.clear();
        if (errorResponse.status === CONSTANTS.errorCodes.UNAUTHORIZED ||
            errorResponse.status === CONSTANTS.errorCodes.TOKEN_EXPIRED ||
            errorResponse.status === CONSTANTS.errorCodes.TOKEN_REQUIRED) {
            localStorage.removeItem('token');
            localStorage.removeItem('token_type');
            localStorage.removeItem('loginUser');
            that.snotifyService.error(messageText, 'Oops..!');
           // window.location.href = '/login';
        }
        else if (errorResponse.status === CONSTANTS.errorCodes.ERROR_CODE_VALIDATION_FAILED) {
            if (error.errors && Object.keys(error.errors).length) {
                messageText = '';
                for (let key in error.errors) {
                    messageText = messageText + ' ' + error.errors[key];
                    if (isSingleErrorReturn) {
                        if (messageVariable) {
                            messageVariable = messageText;
                        }
                        that.message.error = messageText;
                        jQuery('#' + key).focus();
                        return;
                    }
                }
                that.snotifyService.error(messageText, 'Oops..!');
            }
        }
        else if (error.status === CONSTANTS.errorCodes.BAD_REQUEST ||
            error.status === CONSTANTS.errorCodes.NOT_FOUND_HTTP_EXCEPTION ||
            error.status === CONSTANTS.errorCodes.PERMISSION_DENIED ||
            error.status === CONSTANTS.errorCodes.METHOD_NOT_FOUND ||
            error.status === CONSTANTS.errorCodes.ALREADY_EXISTS ||
            error.status === CONSTANTS.errorCodes.DATABASE_INITIALIZATION_FAIL ||
            error.status === CONSTANTS.errorCodes.INVALID_DOMAIN) {
            that.snotifyService.error(messageText, 'Oops..!');
        }
        else {
            that.snotifyService.error(messageText, 'Oops..!');
        }
        if (!messageVariable) {
            if (that.message && that.message) {
                that.message.error = messageText;
            }
        }
        else {
            messageVariable = messageText;
        }
        if (that.isLoading) {
            that.isLoading = false;
        }
    };
}
