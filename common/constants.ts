export const CONSTANTS = {  
  // API: 'https://collegeapi.skolo.co.in:8443/cms/api/',
  // MAINAPI: 'https://collegeapi.skolo.co.in:8443/cms/', 

  // API: 'http://ec2-13-127-125-121.ap-south-1.compute.amazonaws.com:8080/cms/api/',
  // MAINAPI: 'http://ec2-13-127-125-121.ap-south-1.compute.amazonaws.com:8080/cms/',  
 
  // Local Server
  // API: 'http://localhost:9090/cms/api/',
  // MAINAPI: 'http://localhost:9090/cms/', 
  // MINIO: 'http://139.59.0.96:9000/cms/',

  // API: 'https://' + window.location.hostname + ':' + '8443' + '/cms/api/',
  // MAINAPI: 'https://' + window.location.hostname + ':' + '8443' + '/cms/',
  // MINIO: 'https://' + window.location.hostname + ':' + '9000' + '/cms/',

  // prod Servers
  // API: 'http://103.56.30.250:8080/cms/api/',
  // MAINAPI: 'http://103.56.30.250:8080/cms/',
  // MINIO: 'http://103.56.30.250:9000/cms/',

  //  API: 'https://vaagdevicolleges.skolo.in:8443/cms/api/',
  //  MAINAPI: 'https://vaagdevicolleges.skolo.in:8443/cms/',
  //  MINIO: 'https://vaagdevicolleges.skolo.in:9000/cms/',
  //  PaymentRedirectUrl: 'https://vaagdevicolleges.skolo.in:8443/cms/paymentGateway/decryptResponse',
  //  CCAvenueUrl: 'https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction',

  // API: 'https://demoschool.skolo.in:8443/cms/api/',
  // MAINAPI: 'https://demoschool.skolo.in:8443/cms/',
  // MINIO: 'https://demoschool.skolo.in:9000/cms/',


  // Office Linux Server 
    API: 'https://dev.skolo.in:8443/cms/api/',
    MAINAPI: 'https://dev.skolo.in:8443/cms/',
    MINIO: 'https://dev.skolo.in:9000/cms/',
    PaymentRedirectUrl: 'https://dev.skolo.in:8443/cms/paymentGateway/decryptResponse',
    CCAvenueUrl: 'https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction',
  
  // Office Linux Server 
  // API: 'https://office.skolo.in:8443/cms/api/',
  // MAINAPI: 'https://office.skolo.in:8443/cms/',
  // MINIO: 'https://office.skolo.in:9000/cms/',

  // TestLab  Server 
  // API: 'https://testlab.skolo.in:8443/cms/api/',
  // MAINAPI: 'https://testlab.skolo.in:8443/cms/',
  // MINIO: 'https://testlab.skolo.in:9000/cms/',

  // School Demo Server 
  // API: 'https://demoschool.skolo.in:8443/cms/api/',
  // MAINAPI: 'https://demoschool.skolo.in:8443/cms/',
  // MINIO: 'https://demoschool.skolo.in:9000/cms/',

  // Methodist Server 
  // API: 'https://methodist.skolo.in:8443/cms/api/',
  // MAINAPI: 'https://methodist.skolo.in:8443/cms/',
  // MINIO: 'https://methodist.skolo.in:9000/cms/',

  // Production Linux Server 
  // API: 'http://103.56.30.254:8080/cms/api/',
  // MAINAPI: 'http://103.56.30.254:8080/cms/',

  // SCIENT Server 
  // API: 'https://scient.skolo.in:8443/cms/api/',
  // MAINAPI: 'https://scient.skolo.in:8443/cms/',
  // MINIO: 'https://scient.skolo.in:9000/cms/',

  // API: 'https://vaph.skolo.in:8443/cms/api/',
  // MAINAPI: 'https://vaph.skolo.in:8443/cms/',
  // MINIO: 'https://vaph.skolo.in:9000/cms/',

  // SKOLO DEMO
  // API: 'https://demo.skolo.in:8443/cms/api/',
  // MAINAPI: 'https://demo.skolo.in:8443/cms/',
  // MINIO: 'https://demo.skolo.in:9000/cms/',

  // API: 'http://139.59.0.96:8080/cms/api/',
  // MAINAPI: 'http://139.59.0.96:8080/cms/',
  // MINIO: 'http://139.59.0.96:9000/cms/',


  /*---------- AUTHENTICATION --------*/
  login: 'auth/login',
  logout: 'auth/logout',
  resetPassword: 'auth/resetPassword',
  savePassword: 'auth/savePassword',  

  currentCollege: '',

  /*---------- AUTHORIZATION --------*/
  authorization: 'authorization',
  userAccessUrl: 'useraccess',
  securityAuthorizationCrudUrl: 'api/auth/authdetails',

  pgPaytmRequesttUrl: 'pgpaytmrequest',
  pgRedirectUrl: 'pgredirect',

  presentDate: '',
  
  /*----------- ALUMNI ------------*/
  amsCommitteeCrudUrl: 'AmsCommittee',
  amsCommitteeRoleCrudUrl: 'AmsCommitteeRole',
  amsIndustryCrudUrl: 'AmsIndustry',

  /*---------- PAYMENT GATEWAY ----------*/
  initaitePaymentUrl: 'paymentGateway/initiatePayment',
  encryptFormDataUrl: 'paymentGateway/encryptFormData',
  decryptResponseUrl: 'paymentGateway/decryptResponse',
  getOrderDetailsUrl: 'paymentGateway/getOrderDetails',
  getEmpLeaveCountUrl: 'getEmpLeaveCount',
  stgOnlineFeeReceiptsUrl: 'stgOnlineFeereceipts',
  generalPaymentSettingCrudUrl: 'GeneralPaymentSetting',

  /*----------- ROLES -----------*/
  admin: 'ADMIN',

  staff: 'STAFF', 
  student: 'STUDENT',
  accountant: 'ACCOUNTANT',
  management: 'MANAGEMENT',
  nonTeaching: 'NON TEACHING',
  prinicipal: 'PRINCIPAL',
  hod: 'HOD',

  dashboard: '',

  quota: 'QUOTA',
  disability: 'DISABIL',
  title: 'TITLE',
  studentType: 'STDTYPE',
  nationality: 'NATIONALITY',
  religion: 'RELI',
  bloodGroup: 'BGRP',
  language: 'LANG',
  maritalStatus: 'MAST',
  affliation: 'AFFL',
  collegeType : 'CLGTY',
  docType: 'DOCTYPE',
  subjectType: 'SUBTYPE',
  subjectCategory: 'SUBCAT',
  languages: 'LANG',
  gender: 'GENDER',
  modeOfEnquiry: 'MODEENQ',
  knowAboutUs: 'KABTUS',
  EnquiryStatus: 'ENQST',
  employeeStatus: 'EMPSTS',
  employeeState: 'EMPST',
  employeeGrade: 'EMPGRADE',
  employeeType: 'EMPTYPE',
  employeeCat: 'EMPCAT',
  employeeWCat: 'EMPWRKCT',
  teacherFor: 'TCHNGFR',
  appointmentType: 'APPMNT',
  payMode: 'PAYMDE',
  paymentMode: 'PYMNTMDE',
  resident: 'RESD',
  accommodation: 'ACCMDTN',
  isActive: 'isActive',
  sortOrder: 'sortOrder',
  modeOfStudy: 'MDESTDY',
  docFormType: 'DOCFORM',
  lessonStatus: 'LSNSTS',
  feePaymentType: 'FEEPAYTYPE',
  booksBindTypeCategory: 'BINDTYPE',
  periodicalFreq: 'PERIODICALFREQ',
  periodicalType: 'PERIODICALTYPE',
  userTypeCode: 'userTypeCode',
  empDeptId: 'empDeptId',
  processStatusCode: 'PROCSTATUS',
  returnBookCondition: 'RETBOOKCOND',
  bookIssued: 'BOOKISSUED',
  currrencyType: 'CURNCYTYPR',
  bookRegType: 'BOOKREGTYPE',
  bookPRIOR: 'BOOKPRIOR',
  libFineType: 'LIBFINETYPE',
  LIBSETTING: 'LIBSETTING',
  leaveTypeDuration: 'LEAVETYPEDUR',
  counselingStatus: 'CONACTSTAT',
  leaveStatus: 'LEAVEPS',
  hostelFor: 'HSTLFOR',
  hostelRoomType: 'HSTLROOM',
  paymentTypeFreq: 'PMTFREQ',
  audience: 'AUDTYPE',
  relation: 'RELATION', 
  eventStatus: 'EVNTSTS',
  vehicleType: 'VEHTYPE',
  feeFrequency: 'PERIODICALFREQ',
  certificateIssueStatus: 'CERTSTATUS',
  certificateIssueWorkflowStatus: 'CERTWFSTAGE',
  studentStatus: 'STUDENTSTATUS',
  grievance: 'GRV',
  stdAppWf: 'StdAppForm',
  specialFee: 'Spcl Fee',
  paySlipStatus: 'PAYSLIPSTATUS',
  itemType: 'ITEMCATTYPE',
  purchaseStatusType: 'POSTATUS',
  qualifyExamTye: 'QUALIFYEXAM',
  fbInputType: 'FBINPTYP',
  fbUsers: 'FeedBackUsers',
  assignType: 'ASIGNTYPE',
  assignStatus: 'ASIGNSTATUS',
  asgnStatusWf: 'ASSIGNMENT',
  issueStatusUrl: 'ISSUESTATUS',
  CampusComplaintUrl: 'CMPLT',
  examSessionInUrl: 'EXMSESN',
  invlatrDisgTypesUrl: 'INVLATRDISG',
  suggestionForUrl: 'SUGSTNFOR',
  suggestionTypeUrl: 'SUGSTNTYPE',
  specialActivity: 'SPCACT',
  scheduleStatus: 'SCHSTA',
  trainingType: 'PLCMNTTRNGTYP',
  examFeeType: 'EXMFEETYP',
  serviceCategory: 'SERVICECATEGORY',
  empActiveStatus : 'ACTV',
  payerType: 'PYRTYP',
  examSeatStatus: 'EXMSEATS',
  additionalFeeType: 'ADDFEETYPE',
  examResult: 'RESULT',
  internalExamMarksType: 'EIMT',
  revisionType: 'REVISIONTYPE',
  transactionType: 'TRANSACTIONTYPE',
  examPayStatus: 'EXAMPAYSTATUS',
  placementType: 'PSTTYPE',
  placementStatus: 'PLCMNTSTS',
  POType: 'POTYPE',
  transType: 'TRANSTYPE',
  lessonStatusType: 'LESSONSTATUS',
  achievmentLevel: 'ACHVMNTLVL',
  prizes: 'PRIZECAT',
  subjectCatRegistration: 'SUBCATREG',
  subjectRegistrationStatus: 'SUBREGSTS',
  gainfulEngagementUrl: 'GNFENGMNT',
  empRatings: 'EMPRATING',
  wrokLevelFor: 'LVLWRK',
  questionType: 'QUESTYPES',
  profileActivityTypes: 'PROFILEACTIVITYTYPES',
  profileActivities: 'PROFILEACIVITIES',
  ResultValidation: 'RESULTVALID',
  internalPattern: 'INTERNALPATTERN',
  outcome: 'OUTCOME',
  onlineFee: 'ONLINEFEE',

  dateFormate:  'd MMM, y',
  importStudentDetailsUrl: 'importStudentDetails',
  getStgStudentDetailsUrl: 'getStgStudentDetails',
  processStgStudentDetailsUrl: 'processStgStudentDetails',
  importEmployeeDetailsUrl: 'importEmployeeDetails',
  processStgEmployeeDetailsUrl: 'processStgEmployeeDetails',
  subjectRegulationDataSyncUrl: 'subjectregulationDataSync',
 // empStatusCodeUrl: localStorage.getItem('empStatusCode'),
 // studentStatusCodeUrl: localStorage.getItem('studentStatusCode'),


  /*---------- DASHBOARD REPORTS ----------*/
  dashboardReportUrl: 'dashboardreport',
  totalStudentsReportUrl: 'getAllRecords/s_total_students_report',
  totalStudentQuickViewUrl: 'getAllRecords/s_student_QuickView',
  StudentAttendanceCountUrl: 'getAllRecords/s_student_attendance_count',
  studentSchCountUrl: 'getAllRecords/s_student_sch_count',
  employeeCountUrl : 'getAllRecords/s_employee_count',
  detailsFeedbackUrl: 'getAllRecords/s_details_feedback',
  getSurveyStatusUrl: 'getAllRecords/s_get_survey_status',
  employeeAttendanceReportUrl : 'getAllRecords/s_emp_attendance_count',
  staffPayrollListReportUrl: 'getAllRecords/s_staff_payroll_list',
  staffPayrollListFReportUrl : 'getAllRecords/s_staff_payroll_list_f',
  mappedCounselorStudentsUrl: 'mappedcounselorstudents',
  feeTransportCollectionUrl: 'getAllRecords/s_rep_fee_transport_collection',
  feeTransportCollectionDownloadUrl: 'getAllRecordsDownload/s_rep_fee_transport_collection',
  managmentReportUrl: 'getAllRecords/s_db_managment',
  finalInternalMarksUrl: 'getAllRecords/s_get_exam_internal_final_marks',
  examMemoDataPopUrl: 'getAllRecords/s_exam_memodata_pop',
  employeeMonthlyAttendanceReportXslUrl: 'employeemonthlyattendancereport/s_rep_emp_attendanace_detail',
  uploadSubjectUnitUrl: 'uploadsubjectunit',
  examAllotmentDetailsUrl: 'getAllRecords/s_get_exam_allotment_details',
  examStudentResultsUrl: 'getAllRecords/s_get_exam_student_results',

  employeeDetailsByCoursegroupUrl: 'employeedetailsbycoursegroupid',
  transportallocationforstudentUrl: 'transportallocationforstudent',
  hostelallocationforstudentUrl: 'hostelallocationforstudent',
  examMarksEntryStudentsUrl: 'exammarksentrystddetails',

  /*---------- Grievance Category ----------*/
  grievanceCategoryCrudUrl: 'GrievanceCategory',
  grievanceCommitteeCrudUrl: 'GrievanceCommittee',
  complaintsListCrudUrl: 'ComplaintsList',
  committeeMemberCrudUrl: 'CommitteeMember',
  complaintCrudUrl: 'Complaint',
  complaintDetailCrudUrl: 'ComplaintDetail',
  complaintUrl: 'complaint',
  complaintsWfCrudUrl: 'ComplaintsWf',
  complaintDetailUrl: 'complaintdetail',
  transferComplaintUrl: 'transfercomplaint',
  complaintReopenUrl: 'complaintreopen',
  complaintUploadUrl: 'complaintupload',
  classNotesUploadUrl: 'uploadclassnotes',

  /*---------- LEAVE TYPES ----------*/
  leaveTypeCrudUrl: 'LeaveType',
  leaveEntitlementCrudUrl: 'LeaveEntitlement',
  leaveEntitlementByDeptCrudUrl: 'leaveentitlementbydept',
  leaveEntitlementUrl: 'leaveentitlement',
  employeeRunningLeaveCrudUrl: 'EmployeeRunningLeave',
  employeeleaveapplicationUrl: 'employeeleaveapplication', 
  leaveApplicationCrudUrl: 'LeaveApplication',
  EmployeeReportingurl: 'EmployeeReporting',
  assignemployeemanagerUrl: 'assignemployeemanager',
  cancelLeaveApplicationUrl: 'cancelemployeeleaveapplication',
  leaveYearsUrl: 'getYears',
  empAttendanceValidationUrl: 'getAllRecords/s_rep_emp_attendance_validation',
  employeeLeaveSummaryUrl: 'employeeLeaveSummary',

/*---------- LEAVE SUMMARY REPORT ----------*/
  getEmpLeavSummaryReports: 'getAllRecords/s_emp_leave_report',
  empLeavSummaryReportsDownloadUrl: 'getAllRecordsDownload/s_emp_leave_report',

  subjectRegulationUrl: 'subjectregulations',
  groupYrRegulationUrl: 'groupyrregulationdetails',
  groupYrRegulationCrudUrl: 'GroupyrRegulationDetail',
  groupYrRegulationByIdUrl: 'groupyrRegDetailId',
  subjectRegulationCrudUrl: 'Subjectregulation',
  subjectRegulationByIdUrl: 'subjectRegulationId',
  allSubjectResourcesSchedulesUrl: 'allsubjectresourcesschedules',

  /*--------- Pages ------------*/
  pageCrudUrl: 'Page',

  /*--------- ASSIGNMENTS ------------*/
  assignmentCrudUrl: 'Assignment',
  assignmentUrl: 'assignment',
  assignmentUploadUrl: 'assignmentupload',

  /*--------- STUDENT ASSIGNMENTS ------------*/
  studentAssignmentCrudUrl: 'StudentAssignment',
  studentAssignmentUrl: 'studentassignment',
  studentAssignmentUploadUrl: 'studentassignmentupload',

  /*--------- CERTIFICATES ----------*/
  feeCertificateIssueWorkflowUrl: 'feeCertificateIssueWorkflow',
  feecertificateissueUrl: 'feecertificateissue',
  studentTcUrl: 'studenttc',
  feeCertificateIssueUrl: 'FeeCertificateIssue',
  feeCertificateWorkflowUrl: 'FeeCertificateWorkflow',
  empCertificateApprovalUrl: 'getAllRecords/s_get_fee_emp_certificate_approval',
  empCertificateApprovalDownloadUrl: 'getAllRecordsDownload/s_get_fee_emp_certificate_approval',
  feeCertificateWorkflowUpdateUrl: 'feecertificateworkflowupdate',
  generateTransferCertificateUrl: 'generateTransferCertificate',
  studentBonafideReportUrl: 'studentbonafidereport',
  studentPPBonafideReportUrl: 'studentppbonafidereport',
  studentItBonafideReportUrl: 'studentitbonafidereport',
  studentBankBonaFideReportUrl: 'studentbankbonafidereport',
  studentTcBonafideReportUrl: 'studenttcbonafidereport',
  studentCustodianReportUrl: 'studentcustodianreport',
  counselorAttendanceReportUrl: 'getAllRecords/s_rep_counselor_attendance_details',
  stdScholarshiplistUrl: 'getAllRecords/s_std_scholarship_list',
  stdScholarshipDownloadReport: 'getAllRecordsDownload/s_std_scholarship_list',
  counselorFortnightReport: 'getAllRecords/s_get_counselor_report',
  counselorFortnightDownloadReport: 'getAllRecordsDownload/s_get_counselor_report',
  empWorkloadReportUrl: 'getAllRecords/s_rep_emp_workload',
  empWorkloadReportDownloadUrl: 'getAllRecordsDownload/s_rep_emp_workload',
  bookDetailSearchReportUrl: 'bookdetailsearchreport',
  empPayslipGenerationsPDFDownloadUrl: 'empPayslipGenerationsPDFDownload',
  feeCertificateIssueRequestUrl : 'feeCertificateIssueRequest',
  getStudentCertificateDetailsUrl: 'getAllRecords/s_get_student_certificate_details',
  empTodoListUrl: 'getAllRecords/s_rep_emp_todo_list',
  feeCertificateIssueCrudUrl: 'FeeCertificateIssue',
  examStdTxnUploadUrl: 'examstdtxnupload',

  /*----------- UPLOAD -----------*/
  uploadFilesUrl: 'studentapplicationformphotos',
  empUploadFiles: 'employeeapplicationuploads',
  updateStudentFilesUrl: 'updatestudentapplicationformphotos',
  updateStudentUploadUrl: 'studentdetailupload',
  userPagesUrl: 'userpages',

  /*--------- SPECIAl ACTIVITIES ------------*/
  specialActivityCrudUrl: 'SpecialActivity',
  specialActivityUrl: 'specialactivity',
  specialActivityStudentsUrl: 'specialactivitystudents',
  spclActivityAttendanceCrudUrl: 'SpclActivityAttendance',
  spclActivityAttendanceUrl: 'spclActivityAttendance',

  /*--------- COLLEGE CERTIFICATES ------------*/
  CollegeCertificateUrl: 'CollegeCertificate',
  studentProfileCrudUrl: 'StudentProfile',

  /*--------- General Masters ------------*/
  generalMastersUrl: 'generalmasters',

  /*--------- General Details ------------*/
  generalDetailsUrl: 'GeneralDetail',
  generalDetailsByCodeUrl: 'GeneralMaster.generalMasterCode',

  /*--------- General Settings ------------*/
  generalSettingsUrl: 'GeneralSetting',

  /*--------- Organization ------------*/
  organizationsCrudUrl: 'Organization',
  organizationByIdUrl: 'organizationId',
  getDetailsByOrganizationIdUrl: 'Organization.organizationId',
  organizationLogoUploadUrl: 'organizationlogoupload',
  assignSectionToStudentsUrl: 'assignsectiontostudents',

  /*--------- College ------------*/
  collegeCrudUrl: 'College',
  collegeByIdUrl: 'collegeId',
  getDetailsByCollegeIdUrl: 'College.collegeId',
  collegeLogoUploadUrl: 'collegelogoupload',

  /*--------- Campus ------------*/
  campusCrudUrl: 'Campus',
  campusByIdUrl: 'campusId',
  getDetailsByCampusIdUrl: 'Campus.campusId',

  /*--------- Designation ------------*/
  designationCrudUrl: 'Designation',
  designationByIdUrl: 'designationId',

  /*--------- COLLEGE CERTIFICATES ------------*/
  collegeCertificateCrudUrl: 'CollegeCertificate',

  /*--------- Student Category ------------*/
  studentCategoryCrudUrl: 'StudentCategory',
  studentCategoryByIdUrl: 'studentCatId',
  getStudentDetailById: 'studentDetail.studentId',

  /*--------- Department ------------*/
  departmentCrudUrl: 'Department',
  departmentByIdUrl: 'departmentId',
  departmentWiseCounselorUrl : 'departmentwisecounselor',

   /*--------- Department Heads ------------*/
   departmentHeadsCrudUrl: 'EmpDeptHeads',
   departmentHeadByIdUrl: 'empDeptHeadId',

  /*--------- Courses ------------*/
  courseCrudUrl: 'Course',
  courseByIdUrl: 'courseId',
  getDetailsByCourseIdUrl: 'Course.courseId',

  /*--------- Course Type ------------*/
  courseTypeCrudUrl: 'CourseType',
  courseTypeByIdUrl: 'courseTypeId',

  /*--------- Course Group ------------*/
  courseGroupCrudUrl: 'CourseGroup',
  courseGroupByIdUrl: 'courseGroupId',
  courseGroupUrl: 'coursegroups',
  courseYearRegulationUrl: 'getAllRecords/s_course_year_regulations',
  getDetailsByGroupUrl: 'CourseGroup.courseGroupId',

  /*--------- Course Year ------------*/
  courseYearCrudUrl: 'CourseYear',
  courseYearByIdUrl: 'courseYearId',
  getDetailsByCourseYearIdUrl: 'CourseYear.courseYearId',
  courseYearUrl: 'courseyears',

  /*--------- Group Section ------------*/
  groupSectionCrudUrl: 'GroupSection',
  groupsectionUrl : 'groupsection',
  groupSectionByIdUrl: 'groupSectionId',

  /*--------- Academic Year ------------*/
  academicYearCrudUrl: 'AcademicYear',
  academicYearByIdUrl: 'academicYearId',
  getDetailsByAcademicYearIdUrl: 'AcademicYear.academicYearId',

  StudentAcademicbatchUrl: 'StudentAcademicbatch',
  academicBatchUpdateUrl: 'academicbatchupdate',

  /*--------- Financial Year ------------*/
  financialYearCrudUrl: 'FinancialYear',
  financialYearByIdUrl: 'financialYearId',
  financialYearDateUrl: 'financialYearDate',

  /*--------- Building ------------*/
  buildingCrudUrl: 'Building',
  buildingByIdUrl: 'buildingId',
  getDetailsByBuildingIdUrl: 'Building.buildingId',

  /*--------- Block ------------*/
  blockCrudUrl: 'Block',
  blockByIdUrl: 'blockId',
  getDetailsByBlockIdUrl: 'Block.blockId',

   /*--------- Floors ------------*/
   floorCrudUrl: 'Floor',
   floorByIdUrl: 'floorId',
  getDetailsByFloorIdUrl: 'Floor.floorId',

  /*--------- Room Type ------------*/
  roomTypeCrudUrl: 'RoomType',
  roomTypeByIdUrl: 'roomTypeId',
  getDetailsByRoomTypeIdUrl: 'RoomType.roomTypeId',

  /*--------- Caste ------------*/
  casteCrudUrl: 'Caste',
  casteByIdUrl: 'casteId',
  getSubCastesByCasteIdUrl: 'Caste.casteId',

  /*--------- Sub Caste ------------*/
  subCasteCrudUrl: 'SubCaste',
  subCasteByIdUrl: 'subCasteId',

  /*--------- Room ------------*/
  roomCrudUrl: 'Room',
  roomByIdUrl: 'roomId',
  buildingdetailsSearchurl: 'buildingdetails',
  
  /*--------- Regulation ------------*/
  regulationCrudUrl: 'Regulation',
  regulationByIdUrl: 'regulationId',
  getDetailsByRegulationIdUrl: 'Regulation.regulationId',

  /*--------- Country ------------*/
  countriesCrudUrl: 'Country',
  getDetailsByCountryIdUrl: 'Country.countryId',


  /*--------- State ------------*/
  statesCrudUrl: 'State',
  getDetailsByStateIdUrl: 'State.stateId',

  /*--------- District ------------*/
  districtsCrudUrl: 'District',
  getDetailsByDistrictIdUrl: 'District.districtId',

  /*--------- City ------------*/
  citiesCrudUrl: 'City',

  /*--------- Qualification ------------*/
  qualificationCrudUrl: 'Qualification',
  qualificationByIdUrl: 'qualificationId',
  getDetailsByQualificationIdUrl: 'Qualification.qualificationId',

   /*--------- Qualification  Group------------*/
   qualificationGroupCrudUrl: 'QualificationGroup',
   qualificationGroupByIdUrl: 'qualificationGroupId',

    /*--------- Qualification ------------*/
  batchCrudUrl: 'Batch',
  batchByIdUrl: 'BatchId',

    /*--------- WorkflowStages ------------*/
    workflowStageCrudUrl: 'WorkflowStage',
    workflowStageByIdUrl: 'WorkflowStageId',

        /*--------- Banks ------------*/
        banksCrudUrl: 'Bank',
        banksByIdUrl: 'bankId',
        getDetailsBybanksIdUrl: 'Bank.bankId',

        
     /*--------- Document Repository ------------*/
     documentRepositoryCrudUrl: 'DocumentRepository',
     documentRepositoryIdUrl: 'documentRepositoryId',


     /*--------- Week Days ------------*/
     weekdayCrudUrl: 'Weekday',
     weekdayByIdUrl: 'weekdayId',

     /* -------------EVENTS -------------*/
     eventTypeUrl: 'EventType',
     eventTypeIdUrl: 'eventTypeId',
     eventAudienceUrl: 'eventaudience',
     eventUrl: 'events',
     eventsByAudienceUrl: 'eventsByAudience',
     eventsByDateUrl: 'eventsbydate',
     departmentEventCrudUrl: 'DepartmentEvent',
     departmentEventUrl: 'departmentEvent',
     departmentEventUploadUrl: 'departmentEvent/uploadFiles',

      /* -------------Notification And Announcements -------------*/
     notificationUrl : 'Notification',
     notificationIdUrl: 'notificationId',
     notificationsUrl : 'notifications',
     notificationbyaudienceUrl : 'notificationbyaudience',
     notificationUploadUrl: 'notificationupload',

     /*--------- Auto Configuration ------------*/
     configautonumbersCrudUrl: 'configautonumbers',
     configautonumberslistCrudUrl: 'configautonumberslist',
     ConfigAutonumberUrl: 'ConfigAutonumber',
     

      /*--------- General Master Settings ------------*/
      generalMasterCrudUrl: 'generalmasters',
      generalMasterByIdUrl: 'generalMasterId',

       /*--------- General Details Settings ------------*/
       generalDetailsCrudUrl: 'generalmasters',
       generalDetailsByIdUrl: 'generalMasterId',

          /*--------- COLLEGE GENERAL SETTINGS ------------*/
          generalSettingUrl: 'GeneralSetting',
          generalSettingIdUrl: 'generalSettingId',

        /*--------- Subjects ------------*/
       subjectCrudUrl: 'Subject',
       subjectByIdUrl: 'subjectId',
       addSubjectsUrl: 'addSubjects',
       getSubjectByIdUrl: 'Subject.subjectId',
       subjectUrl: 'subject',
       studentSubjectUrl: 'StudentSubject',
       subjectsearchUrl: 'subjectsearch',
       subjectTypeModificationUrl: 'subjecttypemodification',

       importSubjectDetailsUrl: 'importSubjectDetails',
       processStgsubjectDetailsUrl: 'processStgSubjectDetails',

    /*--------- Application Form ------------*/
  studentApplicationFormUrl: 'studentapplicationform',
  studentApplicationListUrl: 'studentapplicationforms',
  studentApplicationUrl: 'StudentApplication',
  studentListUrl: 'studentsList',
  addStudentsListUrl: 'addStudentslist',
  updateRollnoUrl: 'updateRollno',
  studentadmissionreportUrl: 'studentadmissionreport',

   /*--------- Student Enquiry ------------*/
   studentEnquiryUrl: 'StudentEnquiry',
   studentEnquiryByIdUrl: 'enquiryId',

  /*--------- Employee Form ------------*/
  employeeapplicationUrl: 'employeedetails',
  employeeDetailsByIdUrl: 'employeedetailsbyid',
  employeeId: 'employeeId',
  employeeapplicationListUrl: 'employeeList',
  employeeDetailUrl: 'EmployeeDetail',
  employeeDetail : 'employeeDetail.employeeId',
  employeeStatusUrl : 'employeeStatus.generalDetailCode',

  /*----------- StaffCourseyrSubject ----------*/
  staffCourseYrSubjectUrl: 'StaffCourseyrSubject',
  staffcourseyrsubjectsUrl: 'staffcourseyrsubjects',
  subjectcourseyrsUrl: 'subjectcourseyrs',
  staffCourseyrSubjectsCheckUrl: 'staffcourseyrsubjectscheck',

    /*----------- Regulation Subjects ----------*/
  getRegulationSubjectUrl: 'subjectregulations',
  subjectRegulationForCourseUrl: 'subjectregulationsforcoursegroup',

  /*----------- Elective Subjects ----------*/
  subjectsCrudUrl: 'subjects',
  electiveSubjectsRequestUrl: 'electivesubjectrequests',

  /*----------- BOOKS ----------*/
  getBooksUrl: 'Book',
  booksUrl: 'book',
  subjectBookCrudUrl: 'SubjectBook',
  subjectBookUrl: 'subjectbooks',
  subjectregulationUrl: 'Subjectregulation',
  getSubjectBooks: 'Subjectregulation.subjectRegulationId',
  booksearchUrl: 'booksearch',
  bookDetailSearchUrl: 'bookdetailsearch',
  checkaccessionnumberUrl: 'checkaccessionnumber',
  addnewbooksUrl : 'addnewbooks',

  importBookDetailsUrl: 'importBookDetails',
  processStgBookDetailsUrl: 'processStgBookDetails',

   /*----------- SUBJECT UNITS ----------*/
  getSubjectUnitUrl: 'SubjectUnit',
  subjectUnitsCrudUrl: 'subjectunits',
  getSubjectUnitsByIdUrl: 'SubjectUnit.subjectUnitsId',

   /*----------- SUBJECT UNIT TOPICS ----------*/
   getSubjectUnitTopicUrl: 'SubjectUnitTopic',
   subjectUnitTopicUrl: 'subjectunittopic',

  /*----------- STUDENT BATCHES----------*/
  studentBatchCrudUrl: 'Studentbatch',
  getStudentBatch: 'studentbatchId',
  studentdetailUrl: 'studentdetail',
  studentBatchesUrl: 'studentbatches',

  /*----------- BATCH WISE STUDENTS ------------*/
  batchWiseStudentsUrl: 'batchwisestudents',
  batchwiseStudentsElectiveUrl: 'batchwisestudentselective',
  batchWiseStudentUrl: 'BatchwiseStudent',
  batchwiseStudentSecUrl: 'batchwiseStudentSec',
  batchwiseElectiveUrl: 'batchwiseElective',

  /*----------- TIMING SETS ------------*/
  timingSetCrudUrl: 'Timingset',
  timingSetByIdUrl: 'timingsetId',
  timingSetsUrl: 'timingsets',
  timingSetListUrl: 'timingsetslist',

  /*--------- TIMING SLOTS -----------*/
  classTimingsCrudUrl: 'classtimings',
  classTimingByIdUrl: 'classTimingsId',
  classTimingUrl: 'ClassTiming',

  /*---------- WEEKDAY CLASS TIMING ----------*/
  classWeekdayCrudUrl: 'ClassWeekday',
  classWeekdaysUrl: 'classweekdays',
  classWeekdayListUrl: 'classweekdayslist',

  /*-------- TIMETABLE ---------*/
  timetableCrudUrl: 'Timetable',
  timetableByIdUrl: 'timetableId',
  getTimetablesByIdUrl: 'Timetable.timetableId',
  timetablesUrl: 'timetables',
  timetablescurrUrl: 'timetablescurr',

  /*-------- SCHEDULE TIMETABLE ---------*/
  scheduleCrudUrl: 'schedules',
  scheduleEntityUrl: 'Schedule',
  scheduleUrl: 'schedulelistbytimingset',
  scheduleSectionUrl: 'schedulesections',
  schedulesforattendanceUrl: 'schedulesforattendance',

  /*-------------- SUBJECT SYLLABUS PLAN ---------------*/

  staffsyllabusplandetailsUrl : 'staffsyllabusplandetails',
  staffsyllabusplanUrl : 'staffsyllabusplan',
  subjectSyllabusPlanUrl: 'getAllRecords/s_subject_syllabus_plan_report',

  subjectResourceUrl: 'subjectresources',
  subjectResourcesSchedulesUrl: 'subjectresourcesschedules',
  subjectResourceEntityUrl: 'SubjectResource',
  deleteStaffsUrl: 'deletestaffs',

  studentAttendanceListUrl: 'studentabsentlist',
  actualClassesScheduleListUrl: 'actualclassesschedulelist',
  actualClassesScheduleCrudUrl: 'ActualClassesSchedule',
  studentSttendanceDetailsUrl: 'studentattendancedetails',
  studentAttendanceSummaryUrl: 'studentattendancesummary',

  promoteStudentUrl: 'promotestudent',

  collegeCalendarUrl: 'collegecalendar',

  /*------------------- FEE CATEGORY--------------------*/
  feeCategoryUrl: 'FeeCategory',
  feeCategoryIdUrl : 'feeCategoryId',

   /*------------------- FEE PARTICULAR--------------------*/
   feeParticularUrl: 'FeeParticular',
   feeParticularsIdUrl : 'feeParticularsId',

  /*------------------- FEE STRUCTURE--------------------*/
  FeeStructureUrl: 'feestructures',
  FeeStructureByIdUrl: 'feeStructureId',
  FeeStructureCrudUrl: 'FeeStructure',
  studentListByStructureUrl: 'studentsListByStructure',
  feeStructureCourseyrUrl: 'FeeStructureCourseyr',
  feeStructureParticularCrudUrl: 'FeeStructureParticular',

  /*------------------- FEE STUDENT DATA--------------------*/
  feeStructureDataUrl: 'feestudentdata',
  studentFeeListUrl: 'studentfeelist',
  studentfeelistDownloadUrl: 'studentfeelistDownload',
  studentfeeDueDownloadUrl: 'studentfeeDueDownload',
  feeDueDownloadUrl: 'feeDueListDownload',
  busFeeCollectionsUrl: 'busfeecollections',
  empBusFeeCollectionsUrl: 'empbusfeecollections',
  empBusFeeCollectionsDownloadUrl: 'empbusfeecollectionsdownload',
  busFeeCollectionsDownloadUrl: 'busfeecollectionsDownload',
  libraryFeeCollectionsUrl: 'libraryfeecollections',
  libraryFeeCollectionsDownloadUrl: 'libraryfeecollectionsDownload',
  managementStdFeeCollectionsUrl: 'managementstdfeecollections',
  managementStdFeeCollectionsDownloadUrl: 'managementstdfeecollectionsDownload',
  academicyearwiseScholarshipDueListUrl: 'academicyearwisescholarshipduelist',
  academicyearwiseScholarshipDueListDownloadUrl: 'academicyearwisescholarshipduelistDownload',
  feeConsessionListUrl: 'feeconsessionlist',
  feeConsessionListDownloadUrl: 'feeconsessionlistDownload',
  scholarshipStdPrecedingsUrl: 'scholarshipStdPrecedings',
  scholarshipStdPrecedingsDownloadUrl: 'scholarshipStdPrecedingsDownload',
  feeLedgerUrl: 'feeLedger',
  feeLedgerProcedureUrl: 'getAllRecords/s_fee_std_ledger',
  feeLedgerDownloadUrl: 'feeLedgerDownload',
  dayWiseReceiptsNewReportUrl: 'daywisereceiptsNewReport',
  feeDueListUrl: 'getAllRecords/s_fee_due_list',
  feeDueListScholarshipHoldUrl: 'getAllRecords/s_fee_due_list_scholarship_hold',
  scholarshipDueListUrl: 'getAllRecords/s_rep_scholarship_duelist',
  scholarshipduelistDownloadUrl: 'scholarshipduelistDownload',

  particularwise_receiptsUrl: 'getAllRecords/s_rep_fee_particular_wise_receipts',
  particularwise_receiptsDownloadUrl: 'getAllRecordsDownload/s_rep_fee_particular_wise_receipts',

  /*------------------- FEE STUDENT WISE PARTICULARS --------------------*/
  feeStudentWiseParticularsUrl: 'feestudentwiseparticularlists',
  feeStudentWiseParticularsCrudUrl: 'feestudentwiseparticulars',
  feeStudentDataParticularCrudUrl: 'FeeStudentDataParticular',
  feeParticularwisePaymentCrudUrl: 'FeeParticularwisePayment',

  /*------------------- FEE STUDENT WISE DISCOUNTS --------------------*/
  feeStudentWiseDiscountUrl: 'feestudentwisediscounts',

  /*------------------- FEE STUDENT WISE FINES --------------------*/
  feeStudentWiseFinesUrl: 'feestudentwisefines',
  feeStudentDataCrudUrl: 'FeeStudentData',
  feeParticularwisePaymentsforStudentRefundUrl: 'feeparticularwisepaymentsforstudentrefund',

  /*------------------- FEE RECEIPTS --------------------*/
  feeReceiptsUrl: 'feereceipts',
  feeParticularWiseReceiptsUrl: 'feeparticularwisepayments',
  feeReceiptDownloadUrl: 'feeReceiptDownload',
  studentFeeReceiptDownloadUrl: 'studentFeeReceiptDownload',
  // daywisereceiptsUrl: 'getAllRecords/daywise_fee_collection',
  daywisereceiptsUrl: 'getAllRecords/daywise_fee_collection_by_empid',
  dayWiseFeeReportUrl: 'getAllRecords/s_rep_fee_daywise_collection',
  daywisereceiptsPdfDownloadUrl: 'daywisereceiptsPdfDownload',
  daywisereceiptsDownloadUrl: 'getAllRecordsDownload/daywise_fee_collection_by_empid',
  empAattendancePerUrl: 'getAllRecords/s_rep_tt_emp_cls_attendance_per',
  

  /*------------------- EMPLOYEE ATTENDANCE --------------------*/
  empAttendanceEmployeeCrudUrl: 'EmpAttendanceEmployee',
  shiftUrl: 'Shift',
  employeeShiftUrl: 'EmployeeShift',
  shiftdetailsUrl: 'shiftdetails',
  employeeshiftsUrl: 'employeeshifts',


  /*------------------- STUDENT SEARCH --------------------*/
  studentSearchUrl: 'studentsearch',
  detainUrl: 'detain',
  detainstudentslistUrl: 'detainstudentslist',
  detainrecommendedUrl: 'detainrecommended',
  readmissionUrl: 'readmission',
  courseyrsubjectsUrl: 'courseyrsubjects',
  discontinueUrl : 'discontinue',
  passedoutUrl: 'passedout',

  /*--------- Library ------------*/
  libraryDetailsUrl: 'LibraryDetail',
  libraryDetailsByIdUrl: 'libraryId',
  getDetailsByLibraryIdUrl: 'LibraryDetail.libraryId',

      /*--------- Book Purchase ------------*/
  bookPurchaseDetailUrl: 'BookPurchaseDetail',
  bookPurchaseDetailIdUrl: 'bookPurchaseDetailId',
  updateBookDetailsUrl: 'updateBookDetails',

  /*--------- MemberShip ------------*/
  memberShipUrl: 'MemberShip',
  memberShipByIdUrl: 'MemberShipId',
  libraryMemberSearchUrl: 'libraryMemberSearch',
  noLibMembershipUrl: 'nolibmembership',

  /*--------- Supplier Details ------------*/
  supplierDetailsUrl: 'LibSupplierDetail',
  supplierDetailsByIdUrl: 'supplierId',

  /*--------- Book Category ------------*/
  bookCategoryUrl: 'Bookcategory',
  bookCategoryByIdUrl: 'bookcatId',

  /*--------- Author ------------*/
  authorUrl: 'Author',
  authorByIdUrl: 'AuthorId',

  /*--------- Publisher ------------*/
  publisherUrl: 'Publisher',
  publisherByIdUrl: 'PublisherId',

  /*--------- Books ------------*/
  booksByIdUrl: 'BookId',
  addbookUrl: 'addbook',

  bookDetailCrudUrl: 'BookDetail',
  bookDetailsIdUrl: 'bookDetailsId',
  bookDetailsById: 'Book.bookId',

  /*--------- Periodicals ------------*/
  periodicalsUrl: 'Periodical',
  periodicalsByIdUrl: 'periodicalId',
  peridicalUrl: 'periodicals',
  periodicalsDetailUrl: 'PeriodicalsDetail',
  periodicalsDetailByIdUrl: 'periodical.periodicalId',
  periodicalDetIdUrl: 'periodicalDetId',

  /*--------- Rack ------------*/
  RackUrl: 'LibShelve',
  RackByIdUrl: 'shelveId',
  libMemberCrudUrl: 'LibMember',
  libMemberId: 'libMemberId',

  /*------------Book Issued Details------------- */
  bookIssuedetails: 'bookIssuedetails',
  bookIssuedetailUrl: 'BookIssuedetail',
  bookIssuedetailsIdUrl: 'bookIssuedetailsId',
  bookIssuedetailByIdUrl: 'libMember.memberCode',
  bookduelistUrl: 'bookduelist',

  /*------------Reserve Book Details------------- */
  reserveBookUrl: 'ReserveBook',

/*------------Library Setting------------- */
  LibrarySettingUrl: 'LibrarySetting',
  libSettingsIdUrl: 'libSettingsId',
  librarysSettingsUrl: 'libraryssettings',

  /*------------Library Fine Collection------------- */
  libfinecollectionUrl: 'getAllRecords/s_rep_lib_fee_collection',
  getAllRecordsDownloadUrl: 'getAllRecordsDownload',

  /*--------- SMS ------------*/
  smsToStudentsUrl: 'sendsmstostudents',
  getSmsToSbsentStudentsUrl: 'getsmstoabsentstudents',
  sendBulkSmsToMultiUsersUrl: 'sendBulkSmsToMultiUsers',
  smsPatternCrudUrl: 'SmsPattern',
  smsHistoryUrl: 'smshistory',
  smsstaffUrl : 'smsstaff',
  smslogindetailUrl: 'smslogindetail',
  sendBulkSmsUrl: 'sendBulkSms',
  uploadFileForEmailUrl: 'uploadFileForEmail',

  employeeSearchUrl: 'employeesearch',
  
  /*--------- Role ------------*/
  roleCrudUrl: 'Role',
  roleByIdUrl: 'roleId',
  getDetailsByRoleIdUrl: 'Role.roleId',

   /*--------- Role Privilege------------*/
   rolePrivilegeCrudUrl: 'RolePrivilege',
   rolePrivilegeByIdUrl: 'rolePrivilegeId',
   rolePrivilegeListUrl: 'roleprivilegelist',

  /*--------- User Type ------------*/
  userTypeCrudUrl: 'Usertype',
  userTypeByIdUrl: 'usertypeId',
  userDetailsByTypeUrl: 'userdetailsbytype',
  getDetailsByUserTypeIdUrl: 'UserType.userTypeId',
  
  /*--------- Module ------------*/
  moduleCrudUrl: 'Module',
  moduleByIdUrl: 'moduleId',    
  getDetailsByModuleIdUrl: 'Module.moduleId',

  /*--------- Sub Module ------------*/
  subModuleCrudUrl: 'Submodule',
  subModuleByIdUrl: 'subModuleId',
  getDetailsBySubModuleIdUrl: 'SubModule.subModuleId',

  /*--------- Pages ------------*/
  pagesCrudUrl: 'Page',
  pagesByIdUrl: 'pageId',

  /*--------- User ------------*/
  userCrudUrl: 'User',
  userByIdUrl: 'userId',
  getUserByIdUrl: 'user.userId',
  creatingUserForStudentsUrl : 'creatinguserforstudents',

  /*--------- User Role ------------*/
  UserRoleUrl: 'UserRole',
  userroleUrl: 'userrole',
  createuserUrl: 'api/createuser',

  /*--------- Counseling ------------*/
 counselorActivityTypeUrl : 'CounselorActivityType',
 counselorActivityTypeIdUrl : 'counselorActivityTypeId',
 counselorMappingUrl : 'CounselorMapping',
 counselorMappingsListUrl: 'counselormappings',
 counselorIdUrl : 'counselorId',
 counseloractivitysUrl: 'counseloractivitys',

 counselormappingsUrl: 'counselormappings',
 counselormappingswithotabsentiesUrl: 'counselormappingswithotabsenties',
 counselorDetailsUrl: 'counselordetails',

 /*--------- Counselor Activity ------------*/
 counselorActivityUrl: 'CounselorActivity',
 counselorActivityIdUrl: 'counselorActivityId',

  /*------------  Staff Group Section ---------------- */

  staffGroupSectionUrl : 'staffGroupSection',

  staffProxyProcedureUrl: 'getAllRecords/staff_for_proxy',
  attendanceNotTakenStaffUrl: 'getAllRecords/s_rep_attendance_not_taken_staff',
  attendanceNotTakenStaffDownloadUrl: 'getAllRecordsDownload/s_rep_attendance_not_taken_staff',
  staffProxyCrudUrl: 'StaffProxy',
  staffProxiesUrl: 'staffproxies',
  staffProxiesbyEmpDeptUrl: 'staffproxiesbyempdept',
  staffProxiesListUrl: 'staffproxieslist',
  empProxyDetailsUrl: 'empproxydetails',
  proxySubjectUrl: 'proxysubject',

  /*-------------  MY CLASSES --------------*/
  staffSubjectsUrl: 'staffSubjects',
  eventCrudUrl: 'Event',

  /*-------------  MAP FEE STRUCTURES --------------*/
  mapFeeStructureUrl: 'mapfeestructure',

  /*-------------  ELECTIVE GROUP MAPPING --------------*/
  electiveGroupyrMappingUrl: 'ElectiveGroupyrMapping',
  electiveGroupYrMappingListUrl: 'electivegroupyrmapping',

  staffSectionsUrl: 'staffSections',

  schStdApplicationUrl: 'SchStdApplication',
  scholarShipAppCrudUrl: 'scholarshipapp',

  electiveGroupYrMappingNamesUrl: 'electivegroupyrmappingnames',

  /* ------------ PRECEEDINGS-------------------*/
  schPreceedingUrl: 'SchPreceeding',
  schPreceedingsCrudUrl: 'schpreceedings',
  schStdPreceedingUrl: 'schstdpreceeding',
  SchStdPreceedingCrudUrl: 'SchStdPreceeding',

   /* ------------ ACCOUNT PRECEEDINGS-------------------*/
  schAccountsPreceedingUrl: 'SchAccountsPreceeding',
  schPreceedingsByAccPrecedingIdUrl: 'schPreceedingsByAccPrecedingId',
  uploadStdPreceedingsUrl: 'uploadstdpreceedings',
  schStgStdPreceedingUrl: 'SchStgStdPreceeding',
  schstgstdpreceedingsCrudUrl: 'schstgstdpreceedings',
  schAccountsPreceedingsCrudUrl: 'schaccountspreceedings',
  getNullPreceedingsUrl: 'getnullpreceedings',

  libFineCollectionUrl: 'LibFineCollection',

   /* ------------ Transport Detail-------------------*/
   transportDetailUrl: 'TransportDetail',
   transportDetailIdByUrl: 'transportDetailId',
   transportDetailsUrl: 'TransportDetail.transportDetailId',
   vehicleDetailCrudUrl: 'VehicleDetail',
   vehicleDetailIdByUrl: 'vehicleDetailId',
   transportFeePaymentCrudUrl: 'TransportFeePayment',

  /* ------------ Drivers-------------------*/
  driverUrl: 'Driver',
  driverIdByUrl: 'driverId',

   /* ------------ Helpers-------------------*/
   helperUrl: 'Helper',
   helperIdByUrl: 'helperId',

/* ------------ Routes-------------------*/
routeUrl: 'Route',
routeIdByUrl: 'routeId',
routeDetailsByIdUrl: 'Route.routeId',

 /* ------------ Route Stops-------------------*/
 routeStopUrl: 'RouteStop',
 routeStopIdByUrl: 'routeStopId',

 /* ------------ Vehicle Driver-------------------*/
 vehicleDriverUrl: 'VehicleDriver',
 vehicleDriverIdByUrl: 'vehicleDriverId',

/* ------------ Vehicle Routes-------------------*/
vechicleRouterUrl: 'VechicleRoute',
vechicleRouteIdByUrl: 'vechicleRouteId',

/* ------------ Distance Fee-------------------*/
distanceFeerUrl: 'DistanceFee',
distanceFeeIdByUrl: 'distanceFeeId',

 /* ------------ Transport Allocation-------------------*/
 transportAllocationUrl: 'TransportAllocation',
 transportAllocationIdByUrl: 'transportAllocationId',
 transportPaymentUrl: 'transportpayment',

 /* ------------ Hostel Type-------------------*/
 hostelTypeUrl: 'HostelType',
 hostelTypeIdByUrl: 'hostelTypeId',

 /* ------------ Hostel ------------------*/
 hostelUrl: 'HostelDetail',
 hostelIdByUrl: 'hostelId',
 hostelDetailsByIdUrl: 'HostelDetail.hostelId',

  /* ------------ Hostel Room Charge ------------------*/
  hostelRoomChargesIdUrl: 'HostelRoomCharge',
  hostelRoomChargesIdByUrl: 'hstlRoomChargesId',

  /* ------------ Hostel Room ------------------*/
  hostelRoomUrl: 'HostelRoom',
  hostelRoomIdByUrl: 'hstlRoomId',

  /* ------------ Hostel Room  Allocation------------------*/
  hostelRoomAllocationUrl: 'HostelRoomAllocation',
  hostelRoomAllocationIdByUrl: 'hstlRoomAllotId',
  hstlroomallocationUrl: 'hstlroomallocation',
  hostelroomallocationUrl: 'hostelroomallocation',

   /* ------------ Hostel Discounts ------------------*/
   hostelDiscountsUrl: 'HostelDiscount',
   hostelDiscountIdByUrl: 'hstlDiscountId',

/* ------------ Hostel Register ------------------*/
hostelRegisterUrl: 'HostelRegister',
hostelRegisterIdByUrl: 'hstlRegisterId',

/* ------------ Hostel Visitor ------------------*/
hostelVisitorUrl: 'HostelVisitor',
hostelVisitorIdByUrl: 'hstlVisitorId',

/* ------------ PAYROLL CATEGORIES -------------*/
payRollCategoryUrl: 'payrollcategory',
PayrollCategoryCrudUrl: 'PayrollCategory',

/* ------------ PAYROLL GROUPS -------------*/
PayrollGroupCrudUrl: 'PayrollGroup',
payrollGroupUrl: 'payrollgroups',

payrollCategoryGroupUrl: 'PayrollCategoryGroup',

/* ------------ PAYROLL GROUP EMPLOYEES -------------*/
employeePayrollGroupCrudUrl: 'EmployeePayrollGroup',
employeePayrollGroupUrl: 'employeepayrollgroup',
calculatePayrollUrl: 'calculatepayroll',

/* ------------ EMPLOYEE PAYSLIP GENERATION -------------*/
employeePayslipGenerationUrl: 'employeepayslipgenerations',
employeePayslipGenerationCrudUrl: 'EmployeePayslipGeneration',
employeeWisePayslipGenerationsUrl: 'employeewisepayslipgenerations',
payslipGenerationsUrl: 'payslipgenerations',
payslipEmailUrl: 'payslipgenerationmails',
employeePayslipGenerationsByDateUrl: 'employeepayslipgenerationsbydate',

employeePayslipDetailCrudUrl: 'EmployeePayslipDetail',

payrollSettingCrudUrl: 'PayslipSetting',
payslipBranchSettingCrudUrl: 'PayslipBranchSetting',
payslipbrSettingUrl: 'payslipbrsetting',

/* ------------ Hostel Search ------------------*/
roomAllocationSearchUrl: 'roomAllocationSearch',

  /* ------------ BANKS -------------------*/
  bankCrudUrl: 'Bank',

 /* ------------ Reports -------------------*/
  flagUrl: 'flag',
  getStudentsReports: 'getAllRecords/s_get_student_reports',
  studentsReportsDownloadUrl: 'getAllRecordsDownload/s_get_student_reports',

  
   /* ------------ Total No Of Titles Reports -------------------*/
  noOfTitlesUrl: 'getAllRecords/s_rep_lib_titles',
  noOfTitlesDownloadUrl: 'getAllRecordsDownload/s_rep_lib_titles',
  titlesReportUrl: 'titlesreport',
  totalBookReportUrl: 'totalbookreport',
  bookWiseReportUrl: 'bookwisecountreport',

/* ------------ Weekly Time Table -------------------*/
  getTimetableReportsUrl: 'getAllRecords/s_rep_tt_get_timetable_details',
  weeklyTimetableDownloadUrl: 'getAllRecordsDownload/s_rep_tt_get_timetable_details',

/* ------------Book Wise Reports -------------------*/
  bookWiseCountReportUrl: 'getAllRecords/s_rep_lib_book_wise_count',
  bookWiseCountReportDownloadUrl: 'getAllRecordsDownload/s_rep_lib_book_wise_count',

/* ------------ Department Reports -------------------*/
  departmentWiseBooksUrl: 'getAllRecords/s_rep_lib_dept_wise_book_list',
  departmentWiseBooksDownloadUrl: 'getAllRecordsDownload/s_rep_lib_dept_wise_book_list',

   /* ------------Total Books Reports -------------------*/
   totalBooksUrl: 'getAllRecords/s_rep_lib_total_books',
  totalBooksDownloadUrl: 'getAllRecordsDownload/s_rep_lib_total_books',

   /* ------------ Periodical Reports -------------------*/
   periodicalReportsUrl: 'getAllRecords/s_rep_lib_periodcalls',
  periodicalReportsDownloadUrl: 'getAllRecordsDownload/s_rep_lib_periodcalls',

    /* ------------ Book Issue Reports -------------------*/
  bookIssueReportUrl: 'getAllRecords/s_rep_lib_day_wise_book_issue',
  bookIssueReportDownloadUrl: 'getAllRecordsDownload/s_rep_lib_day_wise_book_issue',

   /* ------------ Library Consolidated Reports -------------------*/
   consolidatedReportUrl: 'getAllRecords/s_books_consolidated_report',
   consolidatedReportDownloadUrl: 'getAllRecordsDownload/s_books_consolidated_report',

  /* ------------ Book Retuns Reports -------------------*/
  bookReturnReportUrl: 'getAllRecords/s_rep_day_wise_book_returns',
  bookReturnReportDownloadUrl: 'getAllRecordsDownload/s_rep_day_wise_book_returns',

   /* ------------ Library Fine Collection Reports -------------------*/
  libraruFineCollectionUrl: 'getAllRecords/s_rep_lib_fine_collection',
  libraruFineCollectionDownloadUrl: 'getAllRecordsDownload/s_rep_lib_fine_collection',
  libraruFineCollectionPdfDownloadUrl: 'getAllRecordsPDFDownload/s_rep_lib_fine_collection',

  /* ------------ Student Attendance Percentage Reports -------------------*/
  studentAttendancePercentageReportUrl : 'getAllRecords/s_rep_tt_std_attendance_per',
  studentAttendancePercentageDownloadReportUrl : 'getAllRecordsDownload/s_rep_tt_std_attendance_per',
  studentAttendancePercentageOnlyReportUrl : 'getAllRecords/s_rep_tt_std_tot_attendance_per',

   /* ------------ Student Daywise Attedance Reports -------------------*/
  studentDaywiseAttedanceReportUrl: 'getAllRecords/s_rep_tt_std_daywise_attendance',
  studentDaywiseAttedanceDownloadReportUrl: 'getAllRecordsDownload/s_rep_tt_std_daywise_attendance',
  dialyAttedanceReportUrl: 'getAllRecords/s_rep_tt_std_attendance_Daily',

  /* ------------ Subject Daywise Attedance Reports -------------------*/
 subjectwiseAttedanceReportUrl: 'getAllRecords/s_rep_tt_std_daywise_attendance',
  subjectwiseAttedanceDownloadReportUrl: 'getAllRecordsDownload/s_rep_tt_std_daywise_attendance',
  
     /* ------------ Student Attedance Reports -------------------*/
 studentAttedanceReportUrl: 'getAllRecords/s_rep_tt_std_daywise_attendance',
 studentAttedanceDownloadReportUrl: 'getAllRecordsDownload/s_rep_tt_std_daywise_attendance',

  /* ------------ Subject Wise Attedance Reports -------------------*/
  subjectWiseAttedanceReportUrl: 'getAllRecords/s_rep_tt_std_subwise_attendance',
  subjectWiseDownloadReportUrl: 'getAllRecordsDownload/s_rep_tt_std_subwise_attendance',


   /* ------------ Daily Timetable Reports -------------------*/
   dailyTimetableReportUrl: 'getAllRecords/s_rep_tt_get_timetable_details',
   dailyTimetableDownloadReportUrl: 'getAllRecordsDownload/s_rep_tt_get_timetable_details',

    /* ------------ Coounselor Activity Reports -------------------*/
   counselorActivityReportUrl: 'getAllRecords/s_get_counselor_Activity_report',
   counselorActivityReportDownloadReport: 'getAllRecordsDownload/s_get_counselor_Activity_report',

   empAttendanceReportUrl: 'getAllRecords/s_rep_emp_attendanace_detail',
   empAttendanceReportDownloadReport: 'getAllRecordsDownload/s_rep_emp_attendanace_detail',
   empAttendanceSummaryReportUrl: 'getAllRecords/s_rep_emp_attendanace_summary',
   empAttendanceSummaryReportDownloadReport: 'getAllRecordsDownload/s_rep_emp_attendanace_summary',


   allocateStudentSubjectsUrl: 'getAllRecords/s_pop_std_student_subjects',

   /* ------------ Course Complete Reports -------------------*/
   coursecompletionUrl: 'coursecompletion',

   
   /* ------------ Student Medium of Instructions Reports -------------------*/
   studentmediumofinstructionsUrl: 'studentmediumofinstructions',
   
   /* ------------ No Objection Reports -------------------*/
   noobjectionUrl: 'noobjection',
   

 /* ------------ Payroll Category -------------------*/
 payrollCategoryCrudUrl: 'PayrollCategory',
 payrollCategoryByIdUrl: 'payrollCategoryId',


      /* ------------ Internal Indent-------------------*/
      internalIndentCrudUrl: 'InvInternalIndent',
      internalIndentByIdUrl: 'internalIndId',

 /* ------------ Committee Member-------------------*/
 committeeMemberUrl: 'CommitteeMember',
 committeeMemberByIdUrl: 'committeeMemberId',
 grvCommitteeDetailsUrl: 'grievanceCommittee.grvCommitteeId',

 /* ------------ Inv module starts here -------------------*/

  /* ------------ Store Item-------------------*/
  invStoreItemCrudUrl: 'InvStoreItem',
  invStoreItemByIdUrl: 'storeItemId', 

  /* ------------ Store master Item-------------------*/
  invStoresMasterCrudUrl: 'InvStoresmaster',
  invStoresMasterByIdUrl: 'storeId', 

  /* ------------ Umo master Item-------------------*/
  invUommasterCrudUrl: 'InvUommaster',
  invUommasterByIdUrl: 'uomId',

  /* ------------ Supplier master Item-------------------*/
  invSuppliermasterCrudUrl: 'InvSuppliermaster',
  invSuppliermasterByIdUrl: 'supplierId',

  /* ------------ Brand master Item-------------------*/
  invBrandMasterCrudUrl: 'InvBrandmaster',
  invBrandMasterIdByIdUrl: 'brandmasterId', 

  /* ------------ Item Category -------------------*/
  invItemCategoryCrudUrl: 'InvItemcategory',
  invItemCategoryByIdUrl: 'itemCategoryId',
  getDetailsCategoryByIdUrl: 'InvItemcategory.itemCategoryId',

  /* ------------ Item sub Category -------------------*/
  invItemSubCategoryCrudUrl: 'InvItemsubcategory',
  invItemSubcategoryIdByIdUrl: 'itemSubcategoryId',

  /* ------------ Inv Item master -------------------*/
  invItemmasterCrudUrl: 'InvItemmaster',
  invItemmasterIdUrl: 'itemId',

  /* ------------ Inv ItemopeningStock-------------------*/
  invItemopeningStockCrudUrl: 'InvItemopeningStock',
  invtemopeningStockIdUrl: 'itemopeningStockId',

  /* ------------ Inv Purchase Order-------------------*/
  invPurchaseOrderDomainUrl: 'InvPurchaseOrder',
  invPurchaseOrderUrl: 'invPurchaseOrder',
  invPurchaseOrderByIdUrl: 'poId',
  invPOUrl: 'invpurchaseorder',

  /* ------------ Inv InternalIndent(request) -------------------*/
  invInternalIndentDomainUrl: 'InvInternalIndent',
  invInternalIndentUrl: 'invInternalIndent',
  internalIndIdByIdUrl: 'internalIndId',
 
  /* ------------ Inv InvInternalIssue-------------------*/
    invInternalIssueCrudUrl: 'InvInternalIssue',
    InvInternalIssueUrl: 'invinternalissue',
    invinterIssueIdUrl: 'interIssueId',

  /* ------------ Inv InternalReturnItem-------------------*/
  invInternalReturnItemCrudUrl: 'InvInternalReturnItem',
  invInternalReturnItemUrl: 'invinternalreturn',
  invInternalReturnCrudUrl: 'InvInternalReturn',
  invInterReturnItemIdUrl: 'interReturnItemId',

  InvSrvCrudUrl: 'InvSrv',
  invSrvUrl: 'invsrv',
  invPurchasereturnCrudUrl: 'InvPurchasereturn',
  invPurchaseReturnUrl: 'purchasereturns',

  invStockledgerCrudUrl: 'InvStockledger',

  searchTrakableItems: 'searchTrakableItems',

  /* ------------ Inv module ends here -------------------*/

/* ------------ Email Start here -------------------*/

sendBulkEmailtoEmployeesUrl: 'sendBulkEmailtoEmployees',
sendBulkEmailtoStudentsUrl: 'sendBulkEmailtoStudents',
sendBulkEmailtoSectionStudentsUrl: 'sendBulkEmailtoSectionStudents',
sendBulkEmailtoStudentsByCYurl: 'sendBulkEmailtoStudentsByCY',
sendEmailToAdminurl: 'sendEmailToAdmin',
emailsentlistbydateUrl: 'emailsentlistbydate',
sendBulkEmailtoEmployeesForAttendanceUrl: 'sendBulkEmailtoEmployeesForAttendance',

/* ------------ Email  module ends here -------------------*/

/* ------------ Student Feedback Module Start Here -------------------*/

   /* ------------ Feedback Option Group-------------------*/
   fbOptionGroupUrl: 'FbOptionGroup',
   fbOptionGroupIdUrl: 'fbOptionGroupId',
   fbOptionGroupDetailsByIdUrl: 'FbOptionGroup.fbOptionGroupId',
 
  /* ------------ Feedback Option Choices-------------------*/
   FbOptionchoiceUrl: 'FbOptionchoice',
   fbOptionchoiceIdUrl: 'fbOptionchoiceId',
 
  /* ------------ Feedback Question-------------------*/
  feedbackQuestionUrl: 'FeedbackQuestion',
  fbQuestionIdUrl: 'fbQuestionId',
  importAssessmentUrl: 'assessment/importQuestionsDetails',

   /* ------------ FEEDBACK SUMMARY Reports -------------------*/
   feedbackSummaryReportUrl: 'getAllRecords/s_get_feedback_report',
   feedbackSuggestionReportUrl: 'getAllRecords/s_feedback_suggestion_report',
   studentFeedbackSummaryReportUrl: 'getAllRecords/s_feedback_summary_report_backup',
   feedbackSummaryReportDownloadReport: 'getAllRecordsDownload/s_get_feedback_report',


  /* ------------ Survey Form-------------------*/
  surveyformUrl: 'surveyform',
  surveyFormListUrl: 'SurveyForm',
 surveyFormIdUrl: 'surveyFormId',
 surveyFeedbackUrl: 'surveyfeedback',
 surveyFeedbackListUrl : 'SurveyFeedback',
 getSurveyFeedbackDetails: 'surveyForm.surveyFormId',
 getSurveyFormUrl: 'surveyformdetailsbyenddate',
 surveyFeedbackDetailCrudUrl : 'SurveyFeedbackDetail',
 surveyfeedbackEmpUrl : 'surveyfeedbackEmp',
   
   /* ------------ STUDENT FEEDBACK MODULE ENDS HERE -------------------*/

/*----------------CAMPUS MAINTENANCE MODULE START-------- */

clgManagementIssueUrl: 'ClgManagementIssue',
managementIssueIdUrl: 'managementIssueId',
raisebyEmpUrl: 'raisebyEmp.employeeId',
uploadIssueImageUrl: 'uploadissueimage',

/*-----------CAMPUS MAINTENANCE MODULE END ----------------*/

/*----------------EXAMINATION MODULE START-------- */

examStudentRegistrationCrudUrl: 'ExamStudentRegistration',
examStudentRegistrationPaymentCrudUrl: 'ExamStudentRegistrationPayment',
examStudentRegistrationTransactionCrudUrl: 'ExamStudentRegistrationTransaction',
examStudentRegPaymentUrl: 'examstudentregpayment',
getExamAllotmentDetailsUrl: 'getAllRecords/s_get_exam_allotment_details',
/*------------Exam Master -----------------*/
examMasterUrl: 'ExamMaster',
examIdUrl : 'examId',
getExamMasterDetailsUrl : 'examMaster.examId',
examNotificationUploadUrl: 'examnotificationupload',
studentHallTicketUrl: 'studenthallticket',
examMarksMemoDownloadUrl: 'exammarksmemodownload',
examRevisionSubjectCrudUrl: 'ExamRevisionSubject',

/*------------ EXAM MARKS SETUP ------------*/
examFCARSetupMasterCrudUrl: 'ExamFCARSetupMaster',
examFCARSetupDetailCrudUrl: 'ExamFCARSetupDetail',
examFCARStudentSubMarkCrudUrl: 'ExamFCARStudentSubMark',
examFCARSubjectSyllabusCrudUrl: 'ExamFCARSubjectSyllabus',
examFCARSubjectSyllabusUrl: 'examFCARSubjectSyllabus',
examFCARStudentSubMarkUrl: 'examFCARStudentSubMark',

/*------------Exam Marks Setup  -----------------*/
examMarksSetupUrl: 'ExamMarkssetup',
markssetupIdUrl : 'markssetupId',
exammarkssetupUrl: 'exammarkssetup',

/*------------Exam Grades -----------------*/
examGradeUrl: 'ExamGrade',
examGradesIdUrl : 'examGradesId',

/*------------Exam SESSIONS -----------------*/
examSessionUrl: 'ExamSession',
examSessionIdUrl : 'examSessionId',

/*------------Exam Student -----------------*/
examStudentUrl: 'ExamStudent',
examStudentIdUrl : 'examStdId',
examStudentDetailUrl: 'ExamStudentDetail',
studentExamFeeReceiptDownloadUrl: 'studentExamFeeReceiptDownload',

/*------------EXAM INVIGILATION REMUNERATION -----------------*/
examInvigilationRemunerationUrl: 'ExamInvigilationRemuneration',
examInvgRemunerationIdUrl : 'examInvgRemunerationId',

/*------------EXAM INVIGILATOR ALLOTMENTS -----------------*/
examInvigilationAllotmentUrl: 'ExamInvigilationAllotment',
examInvgAllotmentIdUrl : 'examInvgAllotmentId',
examRoomUrl: 'examRoom.roomId',
examinvigilationallotmentUrl: 'examinvigilationallotment',

/*------------EXAM FEE STRUCTURE -----------------*/
examFeeStructureCrudUrl: 'ExamFeeStructure',
examFeeStructureUrl: 'examfeestructure',
examFeeStructureCourseyrUrl: 'ExamFeeStructureCourseyr',

/*------------EXAM TIMETABLE -----------------*/
examTimetableUrl: 'ExamTimetable',
examTimetableIdUrl : 'examTimetableId',
examTimetablePostUrl: 'examtimetable',
getDetailsByExamTimetableIdUrl: 'ExamTimetable.examTimetableId',
examDetailsByStudentCourseYearUrl: 'examdetailsbystudentcourseyear',
examtTimetableDetailsUrl: 'examtimetabledetails',
examStudentRegistrationUrl: 'examstudentregistration',
examRevisionSubjectUrl: 'examrevisionsubject',

/*------------EXAM TIMETABLE Details-----------------*/
examTimetableDetailUrl: 'ExamTimetableDetail',

/*------------EXAM ALLOTMENT -----------------*/
examRoomAllotmentCrudUrl: 'ExamRoomAllotment',
examRoomAllotmentPostUrl: 'examroomallotment',

  /* ------------ Student EXAM  Reports -------------------*/
  examStudentReportUrl : 'getAllRecords/s_exam_student_report',
  examTimetableReportUrl : 'getAllRecords/s_exam_timetable_report',
  examResultReportUrl : 'getAllRecords/s_exam_result_report',
  // studentAttendancePercentageDownloadReportUrl : 'getAllRecordsDownload/s_rep_std_attendance_per',

  /*------------EXAM COURSE YEAR SUBJECTS -----------------*/
  examStdCourseyrSubjecturl: 'ExamStdCourseyrSubject',
  examStdCourseyrSubUrl: 'examstdcourseyrsub',
  examStudentPostUrl: 'examstudent',
  examStudentCrudUrl: 'ExamStudent',

  /*------------EXAM FEE COLLECTION-----------------*/
  examstdcourseyrsubjectsUrl: 'examstdcourseyrsubjects?',
  examFeeReceiptUrl: 'examfeereceipt',
  examFeeReceiptCrudUrl: 'ExamFeeReceipt',

  examMarksMemoUrl: 'exammarksmemo',
  examMemoMasterCrudUrl: 'ExamMemoMaster',

  /*------------EXAM SUBJECTS-----------------*/
  subjectsforexamUrl: 'subjectsforexam',

  /*------------EXAM ROOMS-----------------*/
  examInvigilatorRoomsUrl: 'examinvigilatorrooms',

  /*------------EXAM STUDENT DETAILS-----------------*/
  examStudentDetailsUrl: 'examstudentdetails',
  examStudentInternalMarksUrl: 'examstudentinternalmarks',
  examStudentInternalMarkCrudUrl: 'ExamStudentInternalMark',
  examStudentMemoSubjectUrl: 'examStudentMemoSubject',
  examtimeTabledetailsByExamDateUrl: 'examtimetabledetailsbyexamdate',
  examSubjectStudentsUrl: 'examsubjectstudents',
  registeredStudentForExamUrl: 'registeredstudentforexam',
  downloadAttendanceNotTakenListUrl: 'downloadattendancenottakenlist/s_rep_attendance_not_taken_staff',

 /*------------EXAM STUDENT ATTENDANCE MARKING-----------------*/
  examroomtudentdetailsUrl: 'examroomstudentdetails',
  exammarksdownloadUrl: 'exammarksdownload',
  uploadexammarksUrl: 'uploadexammarks',
  uploadBulkExamMarksUrl: 'uploadbulkexammarks',
  examBulkMarksPopUrl: 'getAllRecords/s_exam_bulk_marks_pop',
  examHallTicketUrl: 'examhallticket',
  finalInternalMarksListUrl: 'finalinternalmarks',

 /*------------EXAM STUDENT SUBJECTS-----------------*/
 examCourseYearSubjectUrl: 'examCourseYearSubject',
 examFeeRevisionMasterCrudUrl: 'ExamFeeRevisionMaster',
 revaluationUrl: 'revaluation',

/*-----------EXAMINATION MODULE END ----------------*/

/*-----------COLLEGE SUGGESTION MODULE START ----------------*/

collegeSuggestionUrl: 'CollegeSuggestion',
suggestionIdUrl: 'suggestionId',
getSuggestionDetailsUrl: 'suggestionbyUser.userId',
/*------------COLLEGE SUGGESTION  MODULE END ----------------*/


  /* ------------PLACEMENTS AND ACHIVEMENTS MODULE STARTS HERE -------------------*/

          /*-------------TRAINING----------------------*/ 
          trainingUrl : 'Training',
          trainingByIdUrl : 'traningId',
          getTainingDetailsUrl: 'Training.traningId',

          /*-------------TRAINING DETAILS----------------------*/ 
          trainingDetailUrl : 'TrainingDetail',
          trainingDetailByIdUrl : 'traningDetId',
          getTainingDetailsByIdUrl: 'TrainingDetail.traningDetId',

          /*-------------TRAINING STUDENT----------------------*/ 
          trainingStudentUrl : 'TrainingStudent',
          trainingStdIdUrl : 'trainingStdId',
          gettrainingStdIdByIdUrl: 'TrainingStudent.trainingStdId',
  
          /*-------------TRAINING SESSIONS----------------------*/ 
          trainingSessionUrl : 'TrainingSession',
          trainingSessionByIdUrl : 'trainingSessionId',
          trainingSessionDetailsByIdUrl : 'TrainingSession.trainingSessionId',

          /*-------------TRAINING ATTENDANCE----------------------*/ 
          trainingAttendenceUrl : 'trainingstdattend',
          trainingAttendenceDetailsUrl : 'TrainingStudentAttendence',
  
          /*-------------COMPANY CONTACTS----------------------*/ 
          companyContactUrl: 'CompanyContact',
          companyContactDetailsUrl: 'companyContact',
          companyContactByIdUrl : 'companyContactId',
          companyContactDetailUrl : 'CompanyContact.companyContactId',
  
          /*-------------ACHIVEMENT CATEGORIES----------------------*/ 
          achivementCategoryUrl: 'Category',
          achivementCategoryByIdUrl: 'categoryId',
  
          /*-------------ACHIVEMENTS SUBCATEGOTYS----------------------*/ 
          achivementsubcategory: 'SubCategory',
          achivementsubcategoryByIdUrl : 'subCategoryId',
  
          /*-------------ACHIVEMENT----------------------*/ 
          achivementCrudUrl: 'Achievement',
          achivementByIdUrl: 'achievementId',
  
          /*-------------COMPANY----------------------*/ 
          companyUrl: 'Company',
          companyByIdUrl: 'companyId',
          companyDetailUrl : 'Company.companyId',
  
          /*-------------COMPANY MEETING----------------------*/ 
          companyMeetingUrl: 'CompanyMeeting',
          companyMeetingByIdUrl: 'companyMeetingId',
  
          /*-------------PLACEMENT----------------------*/ 
          placementUrl: 'Placement',
          placementByIdUrl: 'PlacementId',
          placementDetailsUrl: 'placementdetails',
          placementDetailsByIdUrl: 'Placement.PlacementId',
          /*-------------PLACEMENT COMPANY----------------------*/ 
          placementCompanyUrl : 'PlacementCompany',
          placementCompanByIdUrl : 'PlacementCompanyId',
  
  
          /*-------------PLACEMENT BROADCAST----------------------*/ 
          placementBroadcastUrl : 'PlacementBroadcast',
          placementBroadcastIdUrl : 'placementBroadcastId',

          /*-------------PLACEMENT STUDENT REGISTRATION----------------------*/ 
          placementStudentRegistrationUrl : 'PlacementStudentRegistration',
          stdregdetailsUrl : 'stdregdetails',
          placementStudentRegIdUrl : 'placementStdRegId',
  

           /*-------------ACHIEVEMENTS----------------------*/ 
           achievementUrl: 'Achievement',

          
  /* ------------Placements and Achivements Module Ends Here -------------------*/


  /* ------------Subject Registration Setup Module Start Here -------------------*/

/*-------------STUDENT SUBJECT REGISTRATION SETUP----------------------*/ 
          stdSubRegistrationSetupUrl : 'StdSubRegistrationSetup',
          stdSubRegSetupIdUrl : 'stdSubRegSetupId',

/*-------------STUDENT SUBJECT REGISTRATION ----------------------*/ 
          stdSubRegistrationUrl: 'StdSubRegistration',
          stdSubregistrationPostUrl: 'stdsubregistration',
          stdsubregUrl: 'stdsubreg',
          courseRegisteredStdForDeptUrl: 'courseregisteredstdfordept',


/*-------------STUDENT SUBJECT REGISTRATION WORKFLOWS ----------------------*/ 
      stdSubRegWorkflowUrl: 'StdSubRegWorkflow',
         

  /* ------------Subject Registration Setup Module Ends Here -------------------*/

  /* ------------Live Classes Module Start Here -------------------*/

  zoomSessionUrl: 'zoomSession',
  liveClassScheduleUrl: 'liveClassSchedule',
  getLiveClassSchedulesUrl: 'liveClassSchedule/findDetails',
  meetingCheckInUrl: 'liveClassSchedule/meetingCheckIn',

   /*----------- DIGITAL LIBRARY -----------*/
   uploadUnitTopicUrl: 'storage/uploadUnitTopic',
   presignedUriUrl: 'storage/presignedUri',
   onlineCoursesCrudUrl: 'OnlineCourses',
   courseLessonSearchUrl: 'courseLessonSearch',
   onlineCoursesDetailsCrudUrl: 'OnlineCourses.onlineCourseId',
   digitalLibraryMemberUrl: 'DigitalLibraryMember',
   courseCategoryUrl: 'CourseCategory',
   employeeDataSecurityCrudUrl: 'EmployeeDataSecurity',

   
  /* ------------ LESSOON TOPIC -------------- */
  courseLessonCrudUrl: 'CourseLesson',
  courseLessonsTopicCrudUrl: 'CourseLessonsTopic',
  courseMemberCrudUrl: 'CourseMember',
  digitalLibraryMemberCrudUrl: 'DigitalLibraryMember',
  memberAssessmentCrudUrl: 'MemberAssessment',
  memberAssessmentDetailsUrl: 'assessment/memberAssessmentDetails',
  membersSyncUrl: 'membersSync',
  onlineCourseAcademicMapUrl: 'onlinecourseacademicmap',
  assessmentCrudUrl: 'Assessment',
  assessmentUrl: 'assessment',
  addQuestionUrl: 'assessment/addQuestion',
  /* ------------Live Classes Module Ends Here -------------------*/

    /* ------------Employee Performance Module Start Here -------------------*/

    empPerfAssessmentQuestionsurl: 'EmpPerfAssessmentQuestions',
    getEmpPerAssessmentUrl: 'getEmpPerAssessment',
    addAssessmentFeedbackUrl: 'addFeedback',
    empPerfAssessmentFeedbackCrudUrl: 'EmpPerfAssessmentFeedback',

      /* ------------Employee Performance Module Ends Here -------------------*/

 dueListFlags: [
   {flagNo: 1, flagName: 'Student_Wise_Summary', flagCode: 'Student Wise Summary'},
   {flagNo: 2, flagName: 'Due_Summary', flagCode: 'Due Summary'},
   {flagNo: 3, flagName: 'Due_Category_Summary', flagCode: 'Due Category Summary'},
   {flagNo: 4, flagName: 'Due_Particular_Summary', flagCode: 'Due Particular Summary'},
 ],

 certificateFor: [
   {id: 'Train Pass.', name: 'Train Pass'},
   {id: 'Bus Pass.', name: 'Bus Pass'},
   {id: 'Bank Loan.', name: 'Bank Loan'},
   {id: 'Scholarship.', name: 'Scholarship'},
  // {id: 'Income Tax.', name: 'Income Tax'},
   {id: 'Conduct.', name: 'Conduct'},
   {id: 'Higher Education.', name: 'Higher Education'},
   {id: 'Job Purpose.', name: 'Job Purpose'},
   {id: 'VISA Purpose.', name: 'VISA Purpose'},
   {id: 'SI Events.', name: 'SI Events'},
   {id: 'Constable Events.', name: 'Constable Events'},
   {id: 'Job In Revenue Department.', name: 'Job In Revenue Department'},
   {id: 'Job In Electrical Department.', name: 'Job In Electrical Department'},
   {id: 'E-Litmus Exam.', name: 'E-Litmus Exam'},
   {id: 'Army Rally.', name: 'Army Rally'},
   {id: 'Sports.', name: 'Sports'},
   {id: 'Passport.', name: 'Passport'},
   {id: 'Internship.', name: 'Internship'},
   {id: 'OTHER', name: 'Other'},
],

  years: [2019, 2020, 2021], 
  
  message: {
      INITIAL: 'Please enter search criteria and hit search button.',
      INTERNAL_ERROR: 'Oops something went wrong..!',
      CON_ERROR: 'Unable to Connect to Server.',
      selectCurYer: 'Course Year are not selected.'
  },

  SMS: {
     notTakenAttendance: 'You forgot to take attendance.',
  },

  patterns: {
    phNo: '[6-9]{1}[0-9]{9}',
    panNo: '^[A-Za-z]{5}[0-9]{4}[A-Za-z]$',
    alphanumaeric: '^[a-zA-Z0-9\\-\\s]+$',
    aadharNo: '[0-9]{12}',
    email: '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'
  },

  validations: {
      email: 'Enter a valid email',
      alphanumaeric: 'Enter a alphanumeric letters no special characters',
      phNo: 'Enter 10 digit number',
      panNo: 'Enter valid pan number',
      aadharNo: 'Enter valid 12 digit of aadhar number'
  },

  toastsOptions: {
      animate: 'flyRight',
      positionClass: 'toast-bottom-right',
      toastLife: 5000
  },

  statusArr: [
      {value: -1, label: 'All'},
      {value: true, label: 'Active'},
      {value: false, label: 'In Active'}
  ],

  examMarksCalType: [
    {id: 1, name: 'Best Of One'},
    {id: 2, name: 'Average'},
],

  pagination: {
      PER_PAGE_DATA: 20,
      MAX_SIZE: 10,
      PAGE_SIZE_OPTIONS: [5, 10, 20, 25, 50, 75, 100]
  },

  errorCodes: {
      BAD_REQUEST: 400,
      UNAUTHORIZED: 401,
      NOT_FOUND_HTTP_EXCEPTION: 404,
      PERMISSION_DENIED: 404,
      METHOD_NOT_FOUND: 405,
      ALREADY_EXISTS: 406,
      DATABASE_INITIALIZATION_FAIL: 407,
      ERROR_CODE_VALIDATION_FAILED: 422,
      INVALID_DOMAIN: 433,
      TOKEN_EXPIRED: 466,
      TOKEN_REQUIRED: 477,
      INTERNAL_SERVER_ERROR: 500
  },

  leaveDays: [
    {name: 'Fore Noon', code: 'F', time: '9 AM to 1 PM'},
    {name: 'After Noon', code: 'A', time: '1 PM to 4 PM'},
    {name: 'Full Day', code: 'H', time: '9 AM to 4 PM'}
  ],

  /* ------- ESI CALCULATION VALUE --------*/
  esi: 0.75,

  monthColors: [
    {id: '01', name: 'JAN', fullName: 'January', code: '#ffdf00'},
    {id: '02', name: 'FEB', fullName: 'Febraury', code: 'green'},
    {id: '03', name: 'MAR', fullName: 'March', code: 'pink'},
    {id: '04', name: 'APR', fullName: 'April', code: 'violet'},
    {id: '05', name: 'MAY', fullName: 'May', code: 'brown'},
    {id: '06', name: 'JUN', fullName: 'June', code: 'red'},
    {id: '07', name: 'JUL', fullName: 'July', code: 'orange'},
    {id: '08', name: 'AUG', fullName: 'August', code: '#dedede'},
    {id: '09', name: 'SEP', fullName: 'September', code: '#dedede'},
    {id: '10', name: 'OCT', fullName: 'October', code: 'rgb(255, 110, 159)'},
    {id: '11', name: 'NOV', fullName: 'November', code: 'rgb(165, 241, 64)'},
    {id: '12', name: 'DEC', fullName: 'December', code: '#dedede'},
  ],

  weekdays: [
    {id: '01', dno: 1, sno: 1, name: 'MON', fullName: 'Monday'},
    {id: '02', dno: 2, sno: 2, name: 'TUE', fullName: 'Tuesday'},
    {id: '03', dno: 3, sno: 3, name: 'WED', fullName: 'Wednesday'},
    {id: '04', dno: 4, sno: 4, name: 'THU', fullName: 'Thursday'},
    {id: '05', dno: 5, sno: 5, name: 'FRI', fullName: 'Friday'},
    {id: '06', dno: 6, sno: 6, name: 'SAT', fullName: 'Saturday'},
    {id: '07', dno: 0, sno: 7, name: 'SUN', fullName: 'Sunday'},
  ],

  subjectColors: [
    {id: 1, color: 'green', value: 'linear-gradient(135deg, #23bdb8 0%, #43e794 100%)'},
    {id: 2, color: 'orange', value: 'linear-gradient(135deg, rgb(244 180 101) 0%, rgb(253 63 63 / 82%) 100%)'},
    {id: 3, color: 'purple-dark', value: 'linear-gradient(45deg, #a52dd8, #e29bf1)'},
    
    {id: 4, color: 'purple', value: 'linear-gradient(135deg, #9a56ff 0%, #e36cd9 100%)'},
    {id: 5, color: 'cyan', value: 'linear-gradient(135deg, rgb(40 113 245), rgb(167 236 132))'},
    {id: 6, color: 'red', value: 'linear-gradient(to right, #a77ffc 0%, #ff6eac 100%)'},
],

// linear-gradient(135deg, rgb(40 113 245), rgb(167 236 132))

  colorCodes: [
    {id: 1, code: '#ffdf00'},
    {id: 2, code: '#27e427'},
    {id: 3, code: 'pink'},
    {id: 4, code: 'violet'},
    {id: 5, code: 'brown'},
    {id: 6, code: 'red'},
    {id: 7, code: 'orange'},
    {id: 8, code: '#dedede'},
    {id: 9, code: '#dedede'},
    {id: 10, code: 'rgb(255, 110, 159)'},
    {id: 11, code: 'rgb(165, 241, 64)'},
    {id: 12, code: '#dedede'},
  ],

};
